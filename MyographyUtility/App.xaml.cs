﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace MyographyUtility
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            //
            base.OnStartup(e);


            //
            ApplicationSetup();

            //
            IoC.Logger.Log("Application starting...", LogLevel.Success);


            //
            Current.MainWindow = new MainWindow();
            Current.MainWindow.Show();
        }

        /// <summary>
        /// 
        /// </summary>
        public void ApplicationSetup()
        {
            //
            IoC.Setup();

            //
            IoC.Kernel.Bind<ILogFactory>().ToConstant(new BaseLogFactory(new[]
            {
                // TODO: Add app settings to configure log location
                //
                new FileLogger("log.txt"),
            }));

            //
            IoC.Kernel.Bind<IFileManager>().ToConstant(new FileManager());

            //
            IoC.Kernel.Bind<ITaskManager>().ToConstant(new TaskManager());

            //
            IoC.Kernel.Bind<IUIManager>().ToConstant(new UIManager());
        }

        
    }
}
