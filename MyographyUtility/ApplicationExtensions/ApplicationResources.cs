﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Media;

namespace MyographyUtility.ApplicationExtensions
{
    /// <summary>
    /// Application resources singleton
    /// </summary>
    public class ApplicationResources
    {
        #region Dependency Properties

        #endregion

        #region Protected Members

        #endregion

        #region Private Members

        /// <summary>
        /// Application resources singleton instance
        /// </summary>
        private static ApplicationResources Instance;

        #endregion

        #region Public Properties

        /// <summary>
        /// All connected devices handles 
        /// </summary>
        public ObservableCollection<int> Handles { get; private set; }

        #endregion

        #region Public Commands

        #endregion

        #region Public Events

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        private ApplicationResources()
        {
            this.Handles = new ObservableCollection<int>();
            this.Handles.CollectionChanged += Handles_CollectionChanged;
        }


        #endregion

        #region Private Callbacks
        private void Handles_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            CollectionExtensions.Sort(this.Handles);
        }
        #endregion

        #region Public Methods

        public static ApplicationResources GetInstance()
        {
            if (Instance == null)
                Instance = new ApplicationResources();

            return Instance;
        }

        public void AddHandle(int handle)
        {
            if (this.Handles.Contains(handle))
                return;

            this.Handles.Add(handle);
        }

        #endregion

        #region Private Methods



        #endregion
    }

    /// <summary>
    /// Collection extensions class
    /// </summary>
    public static class CollectionExtensions
    {
        public static void Sort<T>(this ObservableCollection<T> collection)
        where T : IComparable<T>, IEquatable<T>
        {
            List<T> sorted = collection.OrderBy(x => x).ToList();

            int ptr = 0;
            while (ptr < sorted.Count - 1)
            {
                if (!collection[ptr].Equals(sorted[ptr]))
                {
                    int idx = search(collection, ptr + 1, sorted[ptr]);
                    collection.Move(idx, ptr);
                }

                ptr++;
            }
        }

        public static int search<T>(ObservableCollection<T> collection, int startIndex, T other)
        {
            for (int i = startIndex; i < collection.Count; i++)
            {
                if (other.Equals(collection[i]))
                    return i;
            }

            return -1;
        }
    }
}
