﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyographyUtility
{
    /// <summary>
    /// 
    /// </summary>
    public partial class LineChartControl : UserControl
    {
        #region Dependency Properties

        public static readonly DependencyProperty SeriesProperty =
                               DependencyProperty.Register("Series", 
                                                           typeof(ObservableCollection<Polyline>), 
                                                           typeof(Polyline), 
                                                           new PropertyMetadata(new ObservableCollection<Polyline>(), 
                                                                                new PropertyChangedCallback(OnSeriesCollectionChanged)));

        #endregion

        #region Protected Members

        #endregion

        #region Private Members

        public LineChartGrid Grid;

        #endregion

        #region Public Properties
        public ObservableCollection<Polyline> Series
        {
            get { return (ObservableCollection<Polyline>)GetValue(SeriesProperty); }
            set { SetValue(SeriesProperty, value); }
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        #endregion

        /// <summary>
        /// Default Constructor
        /// </summary>
        #region Constructor
        public LineChartControl()
        {
            InitializeComponent();
            this.Grid = new LineChartGrid(this.ChartArea);
            this.Series.CollectionChanged += Series_CollectionChanged;
        }

        
        #endregion

        #region Public Methods

        #endregion

        #region Private Callbacks
        private static void OnSeriesCollectionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
/*
            foreach (Polyline series in (d as LineChartControl).ChartArea.Children)
                (d as LineChartControl).ChartArea.Children.Remove(series);

            ObservableCollection<LineSeries> newCollection = (ObservableCollection<LineSeries>)e.NewValue;

            if (newCollection.Count > 0)
            {
                foreach (LineSeries series in newCollection)
                    (d as LineChartControl).ChartArea.Children.Add(series.LineSeriesObject);
            }

            (d as LineChartControl).PropertyChanged(d, new PropertyChangedEventArgs(nameof(Series)));
*/
        }

        private void Series_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch(e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var series in e.NewItems)
                        this.ChartArea.Children.Add(series as Polyline);
                       
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (var series in e.OldItems)
                        this.ChartArea.Children.Remove(series as Polyline);
                break;

                default: return;
            }

            this.PropertyChanged(this, new PropertyChangedEventArgs(nameof(this.Series)));
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
