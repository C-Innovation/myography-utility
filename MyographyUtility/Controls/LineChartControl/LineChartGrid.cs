﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace MyographyUtility
{
    /// <summary>
    /// 
    /// </summary>
    public class LineChartGrid
    {
        #region Dependency Properties

        #endregion

        #region Protected Members

        #endregion

        #region Private Members
               
        private Canvas pChartArea;

        /// <summary>
        /// Axis X main grid <see cref="Line"/> collection 
        /// </summary>
        private Line[] pVerticalLines;

        /// <summary>
        /// Axis Y main grid <see cref="Line"/> collection 
        /// </summary>
        private Line[] pHorizontalLines;

        #endregion

        #region Public Properties

        public int VerticalLinesCount { get; private set; } = 10;

        public int HorizontalLinesCount { get; private set; } = 6;

        #endregion

        #region Public Events

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public LineChartGrid(Canvas chartArea)
        {
            new LineChartGrid(chartArea, this.VerticalLinesCount, this.HorizontalLinesCount);
        }

        /// <summary>
        /// Extended constructor
        /// </summary>
        public LineChartGrid(Canvas chartArea, int vLinesCount, int hLinesCount)
        {
            this.pChartArea = chartArea;
            this.VerticalLinesCount = vLinesCount;
            this.HorizontalLinesCount = hLinesCount;
            this.pChartArea.SizeChanged += parentChartAreaSizeChanged;
            gridInit();
        }

        private void parentChartAreaSizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
        {
            gridResize(e.NewSize);
        }

        #endregion

        #region Public Methods

        #endregion

        #region Private Methods

        /// <summary>
        /// Creating new grid object.
        /// </summary>
        /// <param name="xGridLinesCount">Number of X main block sections</param>
        /// <param name="yGridLinesCount">Number of Y main block sections</param>
        private void gridInit()
        {
            //  X Axis Main Grid Block.

            this.pVerticalLines = new Line[VerticalLinesCount];
            DoubleCollection vStrokeDashArray = new DoubleCollection();
            vStrokeDashArray.Add(2);
            vStrokeDashArray.Add(2);
            for (int i = 0; i < this.VerticalLinesCount; i++)
            {
                this.pVerticalLines[i] = new Line();
                this.pVerticalLines[i].Stroke = Brushes.DarkGray;
                this.pVerticalLines[i].StrokeThickness = 2;
                this.pVerticalLines[i].StrokeDashArray = vStrokeDashArray;
                this.pChartArea.Children.Add(this.pVerticalLines[i]);
            }



            //  Y Axis Main Grid Block.

            this.pHorizontalLines = new Line[this.HorizontalLinesCount];
            DoubleCollection hStrokeDashArray = new DoubleCollection();
            hStrokeDashArray.Add(2);
            hStrokeDashArray.Add(2);
            for (int i = 0; i < this.HorizontalLinesCount; i++)
            {
                this.pHorizontalLines[i] = new Line();
                this.pHorizontalLines[i].Stroke = Brushes.DarkGray;
                this.pHorizontalLines[i].StrokeThickness = 2;
                this.pHorizontalLines[i].StrokeDashArray = hStrokeDashArray;
                this.pChartArea.Children.Add(this.pHorizontalLines[i]);
            }

            
        }

        /// <summary>
        /// Resizing grid after window resizing.
        /// </summary>
        /// <param name="NewSize">New ChartArea object size</param>
        private void gridResize(Size NewSize)
        {
           
            for (int i = 0; i < this.VerticalLinesCount; i++)
            {
                this.pVerticalLines[i].X1 = (NewSize.Width / this.VerticalLinesCount) * i;
                this.pVerticalLines[i].X2 = (NewSize.Width / this.VerticalLinesCount) * i;
                this.pVerticalLines[i].Y1 = 0;
                this.pVerticalLines[i].Y2 = NewSize.Height;


            }
            
            for (int i = 0; i < this.HorizontalLinesCount; i++)
            {
                this.pHorizontalLines[i].X1 = 0;
                this.pHorizontalLines[i].X2 = NewSize.Width;
                this.pHorizontalLines[i].Y1 = (NewSize.Height / this.HorizontalLinesCount) * i;
                this.pHorizontalLines[i].Y2 = (NewSize.Height / this.HorizontalLinesCount) * i;

            }
            
        }

        #endregion
    }
}
