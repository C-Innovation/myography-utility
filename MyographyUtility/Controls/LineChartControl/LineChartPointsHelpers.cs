﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MyographyUtility
{
    /// <summary>
    /// 
    /// </summary>
    public class LineChartPointsHelpers
    {
        #region Dependency Properties

        #endregion

        #region Protected Members

        #endregion

        #region Private Members

        private LineChartControl pChart;
        private int pPointCount = 1000;
        private double pXGridSize = 10000;
        private double pYGridSize = 100000;
        private double[] pRawData;
        #endregion

        #region Public Properties

        #endregion

        #region Public Events

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public LineChartPointsHelpers(LineChartControl chartArea)
        {
            this.pChart = chartArea;
            this.pRawData = new double[this.pPointCount];
        }

        /// <summary>
        /// Extended constructor
        /// </summary>
        public LineChartPointsHelpers(LineChartControl chartArea, int pointCount)
        {
            this.pChart = chartArea;
            this.pPointCount = pointCount;
            this.pRawData = new double[this.pPointCount];
        }

        /// <summary>
        /// Extended constructor
        /// </summary>
        public LineChartPointsHelpers(LineChartControl chartArea, int pointCount, double xGridSize, double yGridSize)
        {
            this.pChart = chartArea;
            this.pPointCount = pointCount;
            this.pXGridSize = xGridSize;
            this.pYGridSize = yGridSize;
            this.pRawData = new double[pointCount];
        }

        #endregion

        #region Public Methods

        public PointCollection CalculatePoints(int[] raw_data)
        {
            if (raw_data == null)
                return null;
            double[] convertedRawData = new double[raw_data.Length];

            for(int i = 0; i < raw_data.Length; i++) convertedRawData[i] = (double)raw_data[i];

            return this.CalculatePoints(convertedRawData);
        }

        public PointCollection CalculatePoints(double[] raw_data)
        {
            if (raw_data == null)
                return null;

            Point[] tPoints = new Point[this.pPointCount];

            if(raw_data.Length >= this.pPointCount)
            {
                Array.Copy(raw_data, raw_data.Length - this.pPointCount, this.pRawData, 0, this.pPointCount);
            }
            else
            {
                Array.Copy(this.pRawData, raw_data.Length, this.pRawData, 0, this.pPointCount - raw_data.Length);
                Array.Copy(raw_data, 0, this.pRawData, this.pPointCount - raw_data.Length, raw_data.Length);

            }

            for(int i = 0; i < this.pPointCount; i++)
            {
                double x = (this.pChart.ChartArea.ActualWidth / (this.pXGridSize * this.pChart.Grid.VerticalLinesCount)) * ((this.pXGridSize * this.pChart.Grid.VerticalLinesCount) / this.pPointCount * i);
                double y = (this.pChart.ChartArea.ActualHeight - (this.pChart.ChartArea.ActualHeight / 2)) - ((this.pChart.ChartArea.ActualHeight / (this.pYGridSize * this.pChart.Grid.HorizontalLinesCount)) * (this.pRawData[i]));

                if (y >= this.pChart.ChartArea.ActualHeight)
                {
                    y = this.pChart.ChartArea.ActualHeight - 1;
                }

                if (y <= 0)
                    y = 0;

                tPoints[i] = new Point(x, y);
            }

            return new PointCollection(tPoints);
        }

        public PointCollection UpdatePoints()
        {
            return CalculatePoints(this.pRawData);
        }

        public void SetYGridSize(double ySize)
        {
            this.pYGridSize = ySize;
        }



        #endregion

        #region Private Methods

        #endregion
    }
}
