﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyographyUtility
{
    /// <summary>
    /// 
    /// </summary>
    public partial class LineSeries : UserControl, INotifyPropertyChanged
    {
        #region Private Members

        

        #endregion

        #region Dependency Properties

        public static readonly DependencyProperty ThiknessProperty =
                               DependencyProperty.Register("Thickness", 
                                                           typeof(double), 
                                                           typeof(LineSeries), 
                                                           new PropertyMetadata(1.0, new PropertyChangedCallback(OnThicknessChanged)));

        public static readonly DependencyProperty StrokeColorProperty =
                               DependencyProperty.Register("StrokeColor", 
                                                           typeof(Brush), 
                                                           typeof(LineSeries), 
                                                           new PropertyMetadata(Brushes.Yellow, new PropertyChangedCallback(OnStrokeColorChanged)));

        public static readonly DependencyProperty PointsProperty =
                               DependencyProperty.Register("Points", 
                                                           typeof(PointCollection), 
                                                           typeof(LineSeries), 
                                                           new PropertyMetadata(new PointCollection(),new PropertyChangedCallback(OnPointsChanged)));



        


        new public static readonly DependencyProperty NameProperty =
                                   DependencyProperty.Register("Name", 
                                       typeof(string), 
                                       typeof(LineSeries), 
                                       new PropertyMetadata(""));



        #endregion

        #region Public Properties

        public double Thickness
        {
            get { return (double)GetValue(ThiknessProperty); }
            set { SetValue(ThiknessProperty, value); }
        }

        public Brush StrokeColor
        {
            get { return (Brush)GetValue(StrokeColorProperty); }
            set { SetValue(StrokeColorProperty, value); }
        }

        public PointCollection Points
        {
            get { return (PointCollection)GetValue(PointsProperty); }
            set { SetValue(PointsProperty, value); }
        }

        new public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        #endregion

        #region Default Constructor

        public LineSeries()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        #endregion

        #region Private Callbacks

        private static void OnThicknessChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as LineSeries).LineSeriesObject.StrokeThickness = (double)e.NewValue;
            (d as LineSeries).PropertyChanged(d, new PropertyChangedEventArgs(nameof(Thickness)));
        }

        private static void OnStrokeColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as LineSeries).LineSeriesObject.Stroke = (Brush)e.NewValue;
            (d as LineSeries).PropertyChanged(d, new PropertyChangedEventArgs(nameof(StrokeColor)));
        }

        private static void OnPointsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as LineSeries).PropertyChanged(d, new PropertyChangedEventArgs(nameof(Points)));
        }

        #endregion

        #region Public Methods

        public void SetPoints(Point[] points)
        {
            this.Points = new PointCollection(points);
            this.PropertyChanged(this, new PropertyChangedEventArgs(nameof(Points)));
        }

        public void SetPoints(PointCollection points)
        {
            this.Points = points;
            this.PropertyChanged(this, new PropertyChangedEventArgs(nameof(Points)));
        }

        #endregion
    }
}
