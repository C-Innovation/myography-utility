﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyographyUtility
{
    /// <summary>
    /// Логика взаимодействия для ProgressBar.xaml
    /// </summary>
    public partial class ProgressBar : UserControl
    {
        #region Protected Members

        protected Brush mBrush0 = Brushes.Green;
        protected Brush mBrush50 = Brushes.Green;
        protected Brush mBrush100 = Brushes.Green;

        #endregion

        #region Private Members

        #endregion

        #region Public Structures

        /// <summary>
        /// Progress bar RGB Color structure
        /// </summary>
        public struct ProgressColor
        {
            /// <summary>
            /// Red value
            /// </summary>
            public Int16 Red;

            /// <summary>
            /// Green value
            /// </summary>
            public Int16 Green;

            /// <summary>
            /// Blue value
            /// </summary>
            public Int16 Blue;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Minimum progress value
        /// </summary>
        public double Minimum { get; set; } = 0;

        /// <summary>
        /// Maximum progress value
        /// </summary>
        public double Maximum { get; set; } = 100;

        /// <summary>
        /// Progress value
        /// </summary>
        public double Value
        {
            get => this.Value;

            set
            {
                Debug.Assert(((value <= Maximum) && (value >= Minimum)), "The ProgressBar.Value must be in a range between ProgressBar.Minimum and ProgressBar.Maximum");

                MainBorderContent.Width = (value / (Maximum - Minimum)) * (MainBorder.ActualWidth - 8);
                MainBorderContentText.Text = string.Format("{0:F2} %", ((value / (Maximum - Minimum)) * 100.0));
                MainBorderContent.Background = prepareMainBorderContentBackground(value);
            }
        }

        /// <summary>
        /// Progress bar height
        /// </summary>
        public double ProgressHeight
        {
            get => MainBorder.Height;
            set
            {
                if (value >= 0 && value < double.MaxValue)
                {
                    MainBorder.Height = value;
                    MainBorderContent.Height = (value - 10);
                }
            }
        }
                

        /// <summary>
        /// Start color on 0 %
        /// </summary>
        public ProgressColor Color0 { get; set; } = new ProgressColor { Red = 0, Green = 255, Blue = 0 };

        /// <summary>
        /// Start color on 50 %
        /// </summary>
        public ProgressColor Color50 { get; set; } = new ProgressColor { Red = 0, Green = 255, Blue = 0 };

        /// <summary>
        /// Stop color on 100 %
        /// </summary>
        public ProgressColor Color100 { get; set; } = new ProgressColor { Red = 0, Green = 255, Blue = 0 };

        /// <summary>
        /// Start color on 0 %
        /// </summary>
        public Brush BrushProcent0
        {
            get => this.mBrush0;
            set
            {
                if (value is null)
                    return;

                this.mBrush0 = value;

                Color0 = fromBrush(this.mBrush0);
            }
        }

        /// <summary>
        /// Start color on 50 %
        /// </summary>
        public Brush BrushProcent50
        {
            get => this.mBrush50;
            set
            {
                if (value is null)
                    return;

                this.mBrush50 = value;

                Color50 = fromBrush(this.mBrush50);
            }
        }

        /// <summary>
        /// Stop color on 100 %
        /// </summary>
        public Brush BrushProcent100
        {
            get => this.mBrush100;
            set
            {
                if (value is null)
                    return;

                this.mBrush100 = value;

                Color100 = fromBrush(this.mBrush100);
            }
        }

        /// <summary>
        /// Progress text color
        /// </summary>
        public Brush ProgressForeground
        {
            get => MainBorderContentText.Foreground;
            set
            {
                MainBorderContentText.Foreground = value;
            }
        }

        /// <summary>
        /// Progress bar border brush color
        /// </summary>
        public Brush ProgressBorderBrush
        {
            get => MainBorder.BorderBrush;
            set
            {
                if (value != null)
                    MainBorder.BorderBrush = value;
            }
        }

        #endregion

        #region Public Events

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public ProgressBar()
        {
            InitializeComponent();
            this.ProgressForeground = Brushes.Black;
        }

        #endregion

        #region Public Methods

        #endregion

        #region Private Callbacks

        #endregion

        #region Private Methods

        /// <summary>
        /// Set the progress color
        /// </summary>
        /// <param name="val">Progress value</param>
        /// <returns><see cref="SolidColorBrush"/> Color value</returns>
        private Brush prepareMainBorderContentBackground(double val)
        {
            double proc = ((val / (Maximum - Minimum)) * 100.0);

            Int16 Rstart = 0;
            Int16 Gstart = 0;
            Int16 Bstart = 0;

            Int16 Rstop = 0;
            Int16 Gstop = 0;
            Int16 Bstop = 0;

            double deltaR = 0;
            double deltaG = 0;
            double deltaB = 0;

            byte outR = 0;
            byte outG = 0;
            byte outB = 0;

            if (proc >= 50)
            {
                Rstart = Color50.Red;
                Gstart = Color50.Green;
                Bstart = Color50.Blue;

                Rstop = Color100.Red;
                Gstop = Color100.Green;
                Bstop = Color100.Blue;

                proc -= 50;
            }
            else
            {
                Rstart = Color0.Red;
                Gstart = Color0.Green;
                Bstart = Color0.Blue;

                Rstop = Color50.Red;
                Gstop = Color50.Green;
                Bstop = Color50.Blue;
            }

            deltaR = ((double)Rstop - (double)Rstart) / 50.0;
            deltaG = ((double)Gstop - (double)Gstart) / 50.0;
            deltaB = ((double)Bstop - (double)Bstart) / 50.0;

            outR = (byte)((double)Rstart + (proc * deltaR));
            outG = (byte)((double)Gstart + (proc * deltaG));
            outB = (byte)((double)Bstart + (proc * deltaB));

            string color = string.Format("{0:X2}{1:X2}{2:X2}", outR, outG, outB);

            return (SolidColorBrush)(new BrushConverter().ConvertFrom($"#{color}"));
        }

        private ProgressColor fromHexString(string hexString)
        {
            ProgressColor outColor;

            if (hexString.Length != 6)
                Debugger.Break();

            outColor.Red = Convert.ToByte(hexString.Substring(0, 2), 16);
            outColor.Green = Convert.ToByte(hexString.Substring(2, 2), 16);
            outColor.Blue = Convert.ToByte(hexString.Substring(4, 2), 16);

            return outColor;
        }

        private ProgressColor fromBrush(Brush brush)
        {
            ProgressColor outColor;

            outColor.Red = ((SolidColorBrush)brush).Color.R;
            outColor.Green = ((SolidColorBrush)brush).Color.G;
            outColor.Blue = ((SolidColorBrush)brush).Color.B;

            return outColor;
        }

        #endregion
    }
}
