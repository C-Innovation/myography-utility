﻿using Microsoft.Win32;
using MyographyUtility.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyographyUtility
{
    /// <summary>
    /// Логика взаимодействия для BootProtocolControl.xaml
    /// </summary>
    public partial class BootProtocolControl : UserControl
    {
        #region Dependency Properties

        #endregion

        #region Protected Members

        #endregion

        #region Private Members

        private ushort pMTU = 0;

        #endregion

        #region Public Properties
        public byte ProtocolHandle { get; set; } = 0x00;
        #endregion

        #region Public Events

        public event EventHandler<SystemProtocolViewEventArgs> OnCommandReady;

        public event EventHandler<FirmwareDownloadEventArgs> OnDownloadFirmwareResourcesReady;

        #endregion

        #region Constructor

        public BootProtocolControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods

        public void InitializeDownloadFirmwareProgress(double max)
        {
            this.FirmwareDownloadProgress.Dispatcher.Invoke(() =>
            {
                this.FirmwareDownloadProgress.Minimum = 0;
                this.FirmwareDownloadProgress.Maximum = max;
            });
        }

        public void ReportDownloadFirmwareProgress(double progress)
        {
            this.FirmwareDownloadProgress.Dispatcher.Invoke(() =>
            {
                this.FirmwareDownloadProgress.Value = progress;
            });
            
        }

        public void SetMTU(ushort mtu)
        {
            this.pMTU = mtu;
        }

        #endregion

        #region Private Callbacks
        private void OnCommandClick(object sender, CommandCellEventArgs e)
        {
            // TODO: Add combo box with active handles list.
            //byte handle = getProtocolHandle(ActiveBootProtocols.SelectedItem.ToString());
            //if (this.ProtocolHandle == 0x00)
            //    return;
            BootProtocolTypes protocolType = (BootProtocolTypes)e.ID;
            this.OnCommandReady?.Invoke(this,
                new SystemProtocolViewEventArgs(BootProtocolTestHelpers.PrepareSystemProtocolData((BootProtocolTypes)e.ID,
                (byte)this.PartitionSelection.SelectedIndex, this.ProtocolHandle)));

        }

        private void ClearPartitionCommand_OnCommandCellClick(object sender, CommandCellEventArgs e)
        {
            //byte handle = getProtocolHandle(ActiveBootProtocols.SelectedItem.ToString());
            //if (this.ProtocolHandle == 0x00)
            //    return;
            if (MessageBox.Show($"Partition {e.ID} Will be cleared. Continue?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                this.OnCommandReady?.Invoke(this,
                new SystemProtocolViewEventArgs(BootProtocolTestHelpers.PrepareSystemProtocolData((BootProtocolTypes)e.ID,
                (byte)this.PartitionSelection.SelectedIndex, this.ProtocolHandle)));
        }

        private void DownloadFirmwareCommand_OnCommandCellClick(object sender, CommandCellEventArgs e)
        {
            byte _partitionNumber = (byte)this.PartitionSelection.SelectedIndex;
            if (this.pMTU == 0)
            {
                this.OnCommandReady?.Invoke(this,
                new SystemProtocolViewEventArgs(SystemProtocolTestHelpers.PrepareSystemProtocolData((byte)SystemProtocolTypes.MTU,
                0x01)));
                new Thread(new ThreadStart(() =>
                {
                    ushort timout = 100;
                    while (this.pMTU == 0)
                    {
                        Thread.Sleep(10);
                        timout--;
                        if (timout == 0)
                            break;
                    }
                    if (this.pMTU == 0)
                        MessageBox.Show($"MTU request timeout", "MTU Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    else
                    {
                        downloadFW(_partitionNumber);
                    }
                })).Start();
                
            }
            else
                downloadFW(_partitionNumber);


        }

        #endregion

        #region Private Methods

        private void downloadFW(byte partition)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Binary files (*.bin)|*.bin";
            if (openFileDialog.ShowDialog() == true)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.OnDownloadFirmwareResourcesReady?.Invoke(this,
                    new FirmwareDownloadEventArgs(new FirmwareUpdateResourcesStruct()
                    {
                        PathToBin = openFileDialog.FileName,
                        PartitionNumber = partition,
                        MTU = this.pMTU 
                    }));
                });

            }
            else
            {

            }
        }

        private byte getProtocolHandle(string handleStr)
        {
            byte handle = 0;

            return handle;
        }

        #endregion


    }

    public class FirmwareDownloadEventArgs :EventArgs
    {
        public FirmwareUpdateResourcesStruct FirmwareResources;
        public FirmwareDownloadEventArgs(FirmwareUpdateResourcesStruct firmwareRes)
        {
            this.FirmwareResources = firmwareRes;
        }
    }


}
