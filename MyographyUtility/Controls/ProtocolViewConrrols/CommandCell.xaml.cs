﻿using MyographyUtility.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyographyUtility
{
    /// <summary>
    /// Логика взаимодействия для CommandCell.xaml
    /// </summary>
    public partial class CommandCell : UserControl, INotifyPropertyChanged
    {
        #region Dependency Properties

        private static readonly DependencyProperty IdProperty =
                                DependencyProperty.Register("Id", 
                                typeof(int), 
                                typeof(CommandCell), 
                                new PropertyMetadata(-1, OnIdChanged));
                
        public static readonly DependencyProperty CommandContentProperty =
                               DependencyProperty.Register("CommandContent",
                               typeof(string),
                               typeof(CommandCell),
                               new PropertyMetadata(string.Empty, OnCommandContentChanged));

        #endregion

        #region Protected Members

        #endregion

        #region Private Members

        #endregion

        #region Public Properties

        public int Id
        {
            get { return (int)GetValue(IdProperty); }
            set { SetValue(IdProperty, value); }
        }

        public string CommandContent
        {
            get { return (string)GetValue(CommandContentProperty); }
            set { SetValue(CommandContentProperty, value); }
        }

        #endregion

        #region Public Events

        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        public event EventHandler<CommandCellEventArgs> OnCommandCellClick;

        #endregion

        #region Public Commands

        public ICommand ClickCommand { get; set; }
               
        #endregion

        #region Constructor

        public CommandCell()
        {
            InitializeComponent();
            this.ClickCommand = new RelayCommand(Click);
        }

        #endregion

        #region Public Methods

        #endregion

        #region Private Callbacks
        private static void OnCommandContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as CommandCell).CommandNameText.Text = (string)e.NewValue;
            (d as CommandCell).PropertyChanged(d, new PropertyChangedEventArgs(nameof(CommandContent)));
        }

        private static void OnIdChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as CommandCell).Id = (int)e.NewValue;
            (d as CommandCell).PropertyChanged(d, new PropertyChangedEventArgs(nameof(Id)));
        }

        #endregion

        #region Private Methods

        private void Click()
        {
            this.OnCommandCellClick?.Invoke(this, new CommandCellEventArgs(this.Id, this.Name, this.CommandContent));
        }

        #endregion
    }

    public class CommandCellEventArgs : EventArgs
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string CommandContent { get; set; }


        public CommandCellEventArgs()
        {
            new CommandCellEventArgs(-1, string.Empty, string.Empty);
        }

        public CommandCellEventArgs(int id, string name, string commandContent)
        {
            this.ID = id;
            this.Name = name;
            this.CommandContent = commandContent;
        }
    }
}
