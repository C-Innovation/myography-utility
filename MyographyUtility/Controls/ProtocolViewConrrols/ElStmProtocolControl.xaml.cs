﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyographyUtility
{
    /// <summary>
    /// Логика взаимодействия для ElStmProtocolControl.xaml
    /// </summary>
    public partial class ElStmProtocolControl : UserControl
    {
        #region Dependency Properties

        #endregion

        #region Protected Members

        #endregion

        #region Private Members

        #endregion

        #region Public Properties
        public byte ProtocolHandle { get; set; } = 0x00;
        #endregion

        #region Public Events

        #endregion

        #region Constructor

        public ElStmProtocolControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods

        #endregion

        #region Private Callbacks

        #endregion

        #region Private Methods

        #endregion
    }
}
