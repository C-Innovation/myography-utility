﻿using MyographyUtility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyographyUtility
{
    /// <summary>
    /// Логика взаимодействия для EmgProtocolControl.xaml
    /// </summary>
    public partial class EmgProtocolControl : UserControl
    {
        #region Dependency Properties

        #endregion

        #region Protected Members

        #endregion

        #region Private Members

        #endregion

        #region Public Properties
        public byte ProtocolHandle { get; set; } = 0x00;

        public bool IsRecordingStarted { get; set; } = false;
        #endregion

        #region Public Events

        public event EventHandler<SystemProtocolViewEventArgs> OnCommandReady;

        #endregion

        #region Constructor

        public EmgProtocolControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods

        #endregion

        #region Private Callbacks

        private void OnCommandClick(object sender, CommandCellEventArgs e)
        {
            // TODO: Add combo box with active handles list.
            //byte handle = getProtocolHandle(ActiveBootProtocols.SelectedItem.ToString());
            //if (this.ProtocolHandle == 0x00)
            //    return;
            byte[] data = { 0x00 };
            EmgProtocolTypes emgType = (EmgProtocolTypes)e.ID;
            if (emgType == EmgProtocolTypes.SET_GAIN)
            {
                data[0] = (byte)this.GainSelection.SelectedIndex ;
            }
            else if (emgType == EmgProtocolTypes.SET_MODE)
            {
                data = new byte[] { (byte)this.ModeSelection.SelectedIndex, (byte)this.ModeSelection.SelectedIndex, (byte)this.ModeSelection.SelectedIndex, (byte)this.ModeSelection.SelectedIndex };
            }
            else if (emgType == EmgProtocolTypes.SET_RECORDING)
            {
                this.IsRecordingStarted ^= true;
                data[0] = (byte)(this.IsRecordingStarted ? 0x01 : 0x00);
            }

            this.OnCommandReady?.Invoke(this,
                    new SystemProtocolViewEventArgs(EmgProtocolTestHelpers.PrepareSystemProtocolData(emgType,
                    data, this.ProtocolHandle)));
        }




        #endregion

        #region Private Methods

        #endregion


    }
}
