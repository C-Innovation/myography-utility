﻿using MyographyUtility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyographyUtility
{
    /// <summary>
    /// Логика взаимодействия для SystemProtocolControl.xaml
    /// </summary>
    public partial class SystemProtocolControl : UserControl
    {
        #region Dependency Properties

        #endregion

        #region Protected Members

        #endregion

        #region Private Members

        #endregion

        #region Public Properties

        public byte Handle { get; set; }

        #endregion

        #region Public Events

        public event EventHandler<SystemProtocolViewEventArgs> OnCommandReady;

        #endregion

        #region Constructor

        public SystemProtocolControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Methods

        #endregion

        #region Private Callbacks

        private void OnCommandClick(object sender, CommandCellEventArgs e)
        {
            // TODO: Add combo box with active handles list.
            if (!Enum.IsDefined(typeof(SystemProtocolTypes), e.ID) || (SystemProtocolTypes)e.ID == SystemProtocolTypes.NONE)
                return;
            this.Handle = 0x01;
            byte[] data = getData(e.ID);

            this.OnCommandReady?.Invoke(this,
                    new SystemProtocolViewEventArgs(SystemProtocolTestHelpers.PrepareSystemProtocolData(
                        (byte)e.ID,
                        this.Handle,
                        data)));
            /*
                        CommunicationProtocolTypes protocolType = CommunicationProtocolTypes.NONE;
                        if (e.ID == 7 || e.ID == 8)
                            protocolType = (CommunicationProtocolTypes)ProtocolSelectoin.SelectedIndex;
                        //SystemProtocolTypes protocolType = (SystemProtocolTypes)e.ID;

                        if (e.ID == 10)
                        {
                            if (string.IsNullOrEmpty(TimeInputText.Text))
                                return;

                            this.OnCommandReady?.Invoke(this,
                                new SystemProtocolViewEventArgs(SystemProtocolTestHelpers.PrepareSystemProtocolData((byte)e.ID,
                                this.Handle,
                                protocolType, BitConverter.GetBytes(getTime(TimeInputText.Text)))));
                        }
                        else
                        {
                            this.OnCommandReady?.Invoke(this,
                                new SystemProtocolViewEventArgs(SystemProtocolTestHelpers.PrepareSystemProtocolData((SystemProtocolTypes)e.ID,
                                this.Handle,
                                protocolType)));
                        }
            */
        }

        private void TimeInputText_KeyDown(object sender, KeyEventArgs e)
        {
            Regex regex = new Regex("[D0-D9]");
            string str = e.Key.ToString();
            if ((regex.IsMatch(e.Key.ToString())) ||
                e.Key == Key.Back ||
                e.Key == Key.Delete ||
                (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
            {
                e.Handled = false;
                return;
            }
            e.Handled = true;
        }
        #endregion

        #region Private Methods

        private uint getTime(string timeStr)
        {
            uint time = 0;

            uint.TryParse(timeStr, out time);
            return time;
        }

        private byte[] getData(int id)
        {
            if (id == 7 || id == 8)
            {
                byte[] data = { (byte)ProtocolSelectoin.SelectedIndex };
                return data;
            }
            else if (id == 10)
            {
                byte[] data = BitConverter.GetBytes(getTime(TimeInputText.Text));
                return data;
            }
            else
                return null;
        }

        #endregion


    }

    public class SystemProtocolViewEventArgs : EventArgs
    {
        public CommonProtocolStruct Data { get; set; }
        public SystemProtocolViewEventArgs(CommonProtocolStruct data)
        {
            this.Data = data;
        }
    }
}
