﻿using System;
using System.Windows;
using System.Windows.Markup;

namespace MyographyUtility.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class BootProtocolTestHelpers
    {
        #region Dependency Properties

        #endregion

        #region Protected Members

        #endregion

        #region Private Members

        private static byte pPingNum = 0;

        #endregion

        #region Public Properties

        #endregion

        #region Public Events

        #endregion

        #region Public Methods

        public static CommonProtocolStruct PrepareSystemProtocolData(BootProtocolTypes type, byte data, byte protocolHandle)
        {
            switch (type)
            {
                case BootProtocolTypes.VERSION: return getVersionData(protocolHandle);
                case BootProtocolTypes.PARTITION_SIZE: return getPartitionSizeData(data, protocolHandle);
                case BootProtocolTypes.CLEAR_PARTITION: return getClearPartitionData(data, protocolHandle);
                case BootProtocolTypes.DOWNLOAD: return new CommonProtocolStruct(0);
                case BootProtocolTypes.RUN: return getRunFirmwareData(data, protocolHandle);

                default: return new CommonProtocolStruct(0);
            }
        }

        public static byte[] GetClearPartitionDataBytes(byte partitionNumber, byte protocolHandle)
        {
            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 4,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)BootProtocolTypes.CLEAR_PARTITION,
                Data = new byte[1]
            };
            data.Data[0] = partitionNumber;
            data.CRC16 = get_crc16(data);
            return data.ToByteArray();
        }

        public static byte[] GetFirmwareUpdateDataBytes(byte[] fwStructBytes, byte protocolHandle)
        {
            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = (ushort)(fwStructBytes.Length + 3),
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)BootProtocolTypes.DOWNLOAD,
                Data = new byte[fwStructBytes.Length]
            };
            Array.Copy(fwStructBytes, data.Data, data.Data.Length);
            data.CRC16 = get_crc16(data);
            return data.ToByteArray();
        }

        #endregion

        #region Private Methods

        private static CommonProtocolStruct getVersionData(byte protocolHandle)
        {
            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 3,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)BootProtocolTypes.VERSION,
                Data = new byte[0]
            };
            data.CRC16 = get_crc16(data);
            return data;
        }
         
        private static CommonProtocolStruct getPartitionSizeData(byte partitionNumber, byte protocolHandle)
        {

            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 4,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)BootProtocolTypes.PARTITION_SIZE,
                Data = new byte[1]
            };
            data.Data[0] = partitionNumber;
            data.CRC16 = get_crc16(data);
            return data;
        }

        private static CommonProtocolStruct getClearPartitionData(byte partitionNumber, byte protocolHandle)
        {

            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 4,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)BootProtocolTypes.CLEAR_PARTITION,
                Data = new byte[1]
            };
            data.Data[0] = partitionNumber;
            data.CRC16 = get_crc16(data);
            return data;
        }

        private static CommonProtocolStruct getRunFirmwareData(byte partitionNumber, byte protocolHandle)
        {

            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,   
                Length = 4,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)BootProtocolTypes.RUN,
                Data = new byte[1]
            };
            data.Data[0] = partitionNumber;
            data.CRC16 = get_crc16(data);
            return data;
        }

        private static ushort crc_u16(byte[] bytes)
        {
            ushort crc = 0xFFFF;
            byte i;

            for (int j = 0; j < bytes.Length; j++)
            {
                crc ^= (ushort)(bytes[j] << 8);

                for (i = 0; i < 8; i++)
                    crc = (ushort)(((crc & 0x8000) == 0x8000) ? ((crc << 1) ^ 0x1021) : (crc << 1));
            }
            return crc;



        }

        private static ushort get_crc16(CommonProtocolStruct packet)
        {
            byte[] tmp = new byte[packet.Length + 2];
            Array.Copy(packet.ToByteArray(), 4, tmp, 0, tmp.Length);
            return crc_u16(tmp);
        }

        #endregion
    }
}
