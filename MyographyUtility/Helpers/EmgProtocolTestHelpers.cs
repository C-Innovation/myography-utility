﻿using MyographyUtility.Helpers;
using System;
using System.Windows.Markup;

namespace MyographyUtility
{
    /// <summary>
    /// 
    /// </summary>
    public static class EmgProtocolTestHelpers
    {
        #region Dependency Properties

        #endregion

        #region Protected Members

        #endregion

        #region Private Members

        #endregion

        #region Public Properties

        #endregion

        #region Public Commands

        #endregion

        #region Public Events

        #endregion

        #region Command Methods

        #endregion

        #region Public Methods
        public static CommonProtocolStruct PrepareSystemProtocolData(EmgProtocolTypes type, byte[] data, byte protocolHandle)
        {
            switch (type)
            {
                case EmgProtocolTypes.VERSION: return getVersionData(protocolHandle);
                case EmgProtocolTypes.STATUS: return getStatusData(protocolHandle);
                case EmgProtocolTypes.SET_RECORDING: return getRecordingControlData(protocolHandle, data);
                case EmgProtocolTypes.SET_GAIN: return getGainControlData(protocolHandle, data);
                case EmgProtocolTypes.SET_MODE: return getModeControlData( protocolHandle, data);

                default: return new CommonProtocolStruct(0);
            }
        }
        #endregion

        #region Private Methods
        private static CommonProtocolStruct getVersionData(byte protocolHandle)
        {
            CommonProtocolStruct packet = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 3,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)EmgProtocolTypes.VERSION,
                Data = new byte[0]

            };
            packet.CRC16 = get_crc16(packet);
            return packet;
        }

        private static CommonProtocolStruct getStatusData(byte protocolHandle)
        {
            CommonProtocolStruct packet = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 3,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)EmgProtocolTypes.STATUS,
                Data = new byte[0]

            };
            packet.CRC16 = get_crc16(packet);
            return packet;
        }

        private static CommonProtocolStruct getRecordingControlData(byte protocolHandle, byte[] data = null)
        {
            CommonProtocolStruct packet = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 4,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)EmgProtocolTypes.SET_RECORDING,
                Data = (data is null) ? new byte[0] : data 

            };
            
            packet.CRC16 = get_crc16(packet);
            return packet;
        }

        private static CommonProtocolStruct getGainControlData(byte protocolHandle, byte[] data = null)
        {
            CommonProtocolStruct packet = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 4,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)EmgProtocolTypes.SET_GAIN,
                Data = data

            };
            packet.CRC16 = get_crc16(packet);
            return packet;
        }

        private static CommonProtocolStruct getModeControlData(byte protocolHandle, byte[] data = null)
        {
            CommonProtocolStruct packet = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 7,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)EmgProtocolTypes.SET_MODE,
                Data = data

            };
            packet.CRC16 = get_crc16(packet);
            return packet;
        }


        private static ushort crc_u16(byte[] bytes)
        {
            ushort crc = 0xFFFF;
            byte i;

            for (int j = 0; j < bytes.Length; j++)
            {
                crc ^= (ushort)(bytes[j] << 8);

                for (i = 0; i < 8; i++)
                    crc = (ushort)(((crc & 0x8000) == 0x8000) ? ((crc << 1) ^ 0x1021) : (crc << 1));
            }
            return crc;



        }

        private static ushort get_crc16(CommonProtocolStruct packet)
        {
            byte[] tmp = new byte[packet.Length + 2];
            Array.Copy(packet.ToByteArray(), 4, tmp, 0, tmp.Length);
            return crc_u16(tmp);
        }


        #endregion
    }
}
