﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Input;

namespace MyographyUtility.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class InputCommandTextBoxHelpers : IDisposable
    {
        #region Dependency Properties

        #endregion

        #region Protected Members

        #endregion

        #region Private Members

        TextBox pInputTextBox;

        #endregion

        #region Public Properties

        #endregion

        #region Public Events

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public InputCommandTextBoxHelpers(TextBox inputTextBox)
        {
            this.pInputTextBox = inputTextBox;
            this.pInputTextBox.KeyDown += PInputTextBox_KeyDown;
        }





        #endregion

        #region Private Callbacks

        private void PInputTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if ((Regex.IsMatch(e.Key.ToString(), "^[0-9a-fA-F]+$")) ||
                e.Key == Key.Back ||
                e.Key == Key.Delete ||
                (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
            {
                e.Handled = false;
                return;
            }
            e.Handled = true;
        }

        #endregion

        #region Public Methods

        public byte[] GetInputByteArray()
        {
            if (string.IsNullOrEmpty(this.pInputTextBox.Text))
                return null;

            // TODO: Check the even string length?..
            if (this.pInputTextBox.Text.Length % 2 != 0)
                this.pInputTextBox.Text = this.pInputTextBox.Text.Insert(/*this.pInputTextBox.Text.Length - 1*/0, "0");


            return Enumerable.Range(0, this.pInputTextBox.Text.Length)
                     .Where(x => x % 2 == 0)
                     .Select(x => Convert.ToByte(this.pInputTextBox.Text.Substring(x, 2), 16))
                     .ToArray();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Private Methods

        #endregion
    }
}
