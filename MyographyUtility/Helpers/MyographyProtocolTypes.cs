﻿namespace MyographyUtility.Helpers
{
    /// <summary>
    /// Device protocol types enumeration
    /// </summary>
    public enum CommunicationProtocolTypes
    {
        /// <summary>
        /// Reserved protocol
        /// </summary>
        RESERVED,

        /// <summary>
        /// System protocol
        /// </summary>
        SYSTEM,

        /// <summary>
        /// Boot loader protocol
        /// </summary>
        BOOT,

        /// <summary>
        /// EMG protocol
        /// </summary>
        EMG,

        /// <summary>
        /// Electro stimulator protocol
        /// </summary>
        EL_STM,


        /// <summary>
        /// Unknown protocol
        /// </summary>
        NONE,

    }
    /// <summary>
    /// Device command types enumeration
    /// </summary>
    public enum CommunicationCommandTypes
    {
        /// <summary>
        /// Command from user to device
        /// </summary>
        CMD,
        /// <summary>
        /// Response from device with result of <see cref="Myography.CommunicationCommandTypes.CMD"/> 
        /// </summary>
        RESP,
        /// <summary>
        /// Event response from device
        /// </summary>
        EVENT,
        /// <summary>
        /// Device internal error response
        /// </summary>
        ERROR,

        /// <summary>
        /// Unknown packet type
        /// </summary>
        NONE,

    }

    /// <summary>
    /// System protocol commands enumeration v1.0.
    /// </summary>
    public enum SystemProtocolTypes
    {
        /// <summary>
        /// Protocol version request
        /// </summary>
        VERSION,

        /// <summary>
        /// Device information request
        /// </summary>
        INFO,

        /// <summary>
        /// Device status request
        /// </summary>
        STATUS,

        /// <summary>
        /// Check connection with active device request
        /// </summary>
        PING,

        /// <summary>
        /// Supported protocols request
        /// </summary>
        PROTOCOLS,

        /// <summary>
        /// Active device reset request
        /// </summary>
        RESET,

        /// <summary>
        /// Switch to boot mode request
        /// </summary>
        IN_BOOT,

        /// <summary>
        /// Open protocol request
        /// </summary>
        OPEN_PROTOCOL,

        /// <summary>
        /// Close protocol request
        /// </summary>
        CLOSE_PROTOCOL,

        /// <summary>
        /// Minimum data block size request
        /// </summary>
        MTU,

        /// <summary>
        /// Set time correction for synchronization request
        /// </summary>
        SET_TIME_CORRECTION,



        /// <summary>
        /// Unknown command
        /// </summary>
        NONE,

        /*
        /// <summary>
        /// Запрос версии протокола.
        /// </summary>
        VERSION,
        /// <summary>
        /// Запрос перечня поддерживаемых протоколов.
        /// </summary>
        PROTOCOLS,
        /// <summary>
        /// Запрос информации об устройстве.
        /// </summary>
        INFO,
        /// <summary>
        /// Запрос статуса устройства.
        /// </summary>
        STATUS,
        /// <summary>
        /// Запрос на синхронизацию устройства.
        /// </summary>
        SYNC,
        /// <summary>
        /// Сброс устройства.
        /// </summary>
        RESET,
        /// <summary>
        /// Запрос на проверку, что устройство активно.
        /// </summary>
        PING,
        /// <summary>
        /// Переход в режим загрузчика.
        /// </summary>
        IN_BOOT,
        /// <summary>
        /// Запрос всех доступных команд.
        /// </summary>
        ALL,
        /// <summary>
        /// Запуск работы с устройством.
        /// </summary>
        START,
        /// <summary>
        /// Остановка работы с устройством.
        /// </summary>
        STOP,
        */
    }

    /// <summary>
    /// Custom boot-loader protocol commands enumeration
    /// </summary>
    public enum BootProtocolTypes
    {
        /// <summary>
        /// Protocol version request
        /// </summary>
        VERSION,

        /// <summary>
        /// MCU flash memory partition size request
        /// </summary>
        PARTITION_SIZE,

        /// <summary>
        /// MCU flash memory clear partition request
        /// </summary>
        CLEAR_PARTITION,

        /// <summary>
        /// Download firmware fragment request
        /// </summary>
        DOWNLOAD,

        /// <summary>
        /// Run hardware application
        /// </summary>
        RUN,


        /// <summary>
        /// Unknown command
        /// </summary>
        NONE,
    }

    /// <summary>
    /// Common firmware packet types for update protocol
    /// </summary>
    public enum DownloadFirmwarePacketTypes
    {
        /// <summary>
        /// First packet marker
        /// </summary>
        FIRST,

        /// <summary>
        /// Middle packet marker
        /// </summary>
        MIDDLE,

        /// <summary>
        /// Last packet marker
        /// </summary>
        LAST,
    }

    /// <summary>
    /// EMG amplifier protocol commands enumeration
    /// </summary>
    public enum EmgProtocolTypes
    {
        /// <summary>
        /// Protocol version request
        /// </summary>
        VERSION,

        /// <summary>
        /// EMG amplifier device status 
        /// </summary>
        STATUS,

        /// <summary>
        /// Data registration control command
        /// </summary>
        SET_RECORDING,

        /// <summary>
        /// Gain setup control command
        /// </summary>
        SET_GAIN,

        /// <summary>
        /// Mode setup control command
        /// </summary>
        SET_MODE,


        /// <summary>
        /// Unknown command
        /// </summary>
        NONE,
    }

    /// <summary>
    ///  EMG amplifier protocol events enumeration
    /// </summary>
    public enum EmgProtocolEventTypes
    {
        /// <summary>
        /// EMG data packet
        /// </summary>
        EMG_EVENT,

        /// <summary>
        /// The RX measurement state machine marker packet
        /// </summary>
        RX_EVENT,

        /// <summary>
        /// Unknown event
        /// </summary>
        NONE,
    }

    /// <summary>
    /// Команды протокола крейта v0.1.
    /// </summary>
    public enum CrateProtocolTypes
    {
        /// <summary>
        /// Запрос на включение передатчика ИК синхрометок.
        /// </summary>
        CRATE_IR_ENABLE,
        /// <summary>
        /// Запрос на выключение передатчика ИК синхрометок.
        /// </summary>
        CRATE_IR_DISABLE,
        /// <summary>
        /// Запрос на включение питания на слот.
        /// </summary>
        CRATE_SLOT_ENABLE,
        /// <summary>
        /// Запрос на выключение питания на слот.
        /// </summary>
        CRATE_SLOT_DISABLE,
        /// <summary>
        /// Запрос на рестарт слота.
        /// </summary>
        CRATE_SLOT_RESET,
        /// <summary>
        /// Запрос на перевод слота в загрузчик.
        /// </summary>
        CRATE_SLOT_TO_BOOT,
        /// <summary>
        /// Запрос версии протокола.
        /// </summary>
        CRATE_VERSION,
    }

    /// <summary>
    /// Команды и события протокола ЭМГ усилителя v1.0.
    /// </summary>
    public enum AmpProtocolTypes
    {
        /// <summary>
        /// Запрос статуса устройства.
        /// </summary>
        AMP_STATUS,
        /// <summary>
        /// Управление регистрацией ЭМГ.
        /// </summary>
        AMP_RECORDING,
        /// <summary>
        /// Управление усилением.
        /// </summary>
        AMP_GAIN,
        /// <summary>
        /// Команда: перевод устройства в режим измерения подэлектродного сопротивления. Событие: получение данных канала подэлектродного сопротивления.
        /// </summary>
        AMP_IMPEDANCE,
        /// <summary>
        /// Перевод устройства в режим внутреннего шунтирования входа.
        /// </summary>
        AMP_SHORT,
        /// <summary>
        /// Команда: перевод устройства в режим регистрации ЭМГ. Событие: получение данных канала ЭМГ.
        /// </summary>
        AMP_EMG,
        /// <summary>
        /// Запрос версии протокола.
        /// </summary>
        AMP_VERSION,
        /// <summary>
        /// Данные канала смещения.
        /// </summary>
        AMP_DC,
        /// <summary>
        /// Данные канала идентификаторов коннекторов.
        /// </summary>
        AMP_ID,
    }

    /// <summary>
    /// Команды и события протокола электростимулятора v0.5.
    /// </summary>
    public enum EstmProtocolTypes
    {
        /// <summary>
        /// Запрос на отмену выполняемой стимуляции.
        /// </summary>
        EL_STM_BREAK,
        /// <summary>
        /// Запрос на установку параметров одиночного стимула.
        /// </summary>
        EL_STM_STIMUL_PARAM,
        /// <summary>
        /// Запрос на запуск однократного стимула.
        /// </summary>
        EL_STM_MAKE_STIMUL,
        /// <summary>
        /// Запрос на загрузку сценария стимуляции.
        /// </summary>
        EL_STM_SCRIPT_DOWNLOAD,
        /// <summary>
        /// Запрос на запуск сценария стимуляции.
        /// </summary>
        EL_STM_SCRIPT_START,
        /// <summary>
        /// Запрос версии протокола.
        /// </summary>
        EL_STM_GET_VERSION,
        /// <summary>
        /// Событие заверешения стимула (как одиночного так и в рамках сценария).
        /// </summary>
        EL_STM_STIMUL,
        /// <summary>
        /// Событие ошибки электростимулятора. Типы ошибки: overcurrent; time in past; stimulator error.
        /// </summary>
        EL_STM_ERROR,
        /// <summary>
        /// Событие канала идентификации.
        /// </summary>
        ESTM_ID
    }

    /// <summary>
    /// Команды протокола функциональной клавиатуры v0.1.
    /// </summary>
    public enum FkProtocolTypes
    {
        /// <summary>
        /// Запрос на отправку блока данных в планшетный ПК.
        /// </summary>
        FK_SEND_TO_TABLET,
        /// <summary>
        /// Событие аппаратной кнопки ФК.
        /// </summary>
        BUTTON,
        /// <summary>
        /// Событие энкодера ФК.
        /// </summary>
        ENCODER,
        /// <summary>
        /// Событие от планшетного ПК.
        /// </summary>
        TABLET_PC
    }
}
