﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MyographyUtility.Helpers
{

    #region System Protocol Structure

    /// <summary>
    /// System protocol structure with <see cref="Marshal"/> mapping
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public struct CommonProtocolStruct
    {
        #region Public Constants

        /// <summary>
        /// Header size in bytes.
        /// </summary>
        public const ushort HEADER_SIZE = 11;

        /// <summary>
        /// Header static preamble.
        /// </summary>
        public const uint HEADER_PREAMBLE = 0x4E6F694D;//{ 0x4D, 0x69, 0x6F, 0x4E};

        #endregion

        #region Public Properties

        /// <summary>
        /// Header preamble.
        /// </summary>
        [MarshalAs(UnmanagedType.U4)]
        public uint Preamble;

        /// <summary>
        /// Length field of the system protocol packet
        /// </summary>
        [MarshalAs(UnmanagedType.U2)]
        public ushort Length;

        /// <summary>
        /// Handle field of the system protocol packet
        /// </summary>
        [MarshalAs(UnmanagedType.U1)]
        public byte ProtocolHandle;

        /// <summary>
        /// Packet type field of the system protocol packet
        /// </summary>
        [MarshalAs(UnmanagedType.U1)]
        public CommunicationCommandTypes PacketType;

        /// <summary>
        /// Op code or event field of the system protocol packet
        /// </summary>
        [MarshalAs(UnmanagedType.U1)]
        public byte OpCode;

        /// <summary>
        /// Data field of the system protocol packet
        /// </summary>
        [MarshalAs(UnmanagedType.ByValArray, ArraySubType = UnmanagedType.U1)]
        public byte[] Data;


        /// <summary>
        /// Packet checksum.
        /// </summary>
        [MarshalAs(UnmanagedType.U2)]
        public ushort CRC16;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="length">Size of data field</param>
        public CommonProtocolStruct(ushort length)
        {
            this.Preamble = HEADER_PREAMBLE;
            this.Length = (ushort)(length + HEADER_SIZE - sizeof(ushort));
            this.ProtocolHandle = 0x00;
            this.PacketType = CommunicationCommandTypes.NONE;
            this.OpCode = 0xff;
            this.Data = new byte[length];
            this.CRC16 = 0xFFFF;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Convert the byte array to <see cref="CommonProtocolStruct"/> object 
        /// </summary>
        /// <param name="buffer">Input data from memory</param>
        /// <returns></returns>
        public static CommonProtocolStruct FromByteArray(byte[] buffer)
        {
            CommonProtocolStruct systemData = new CommonProtocolStruct()
            {
                Preamble = BitConverter.ToUInt32(buffer, 0),
                Length = BitConverter.ToUInt16(buffer, 4),
                ProtocolHandle = buffer[6],
                PacketType = (CommunicationCommandTypes)buffer[7],
                OpCode = buffer[8],
                Data = new byte[buffer.Length - HEADER_SIZE]
            };
            Array.Copy(buffer, HEADER_SIZE - sizeof(ushort), systemData.Data, 0, systemData.Data.Length);
            systemData.CRC16 = BitConverter.ToUInt16(buffer, buffer.Length - sizeof(ushort));
            return systemData;
        }

        /// <summary>
        /// Convert the <see cref="CommonProtocolStruct"/> object to byte array
        /// </summary>
        /// <returns>Result of conversion as byte array </returns>
        public byte[] ToByteArray()
        {
            var _size = this.Data.Length + HEADER_SIZE;
            var systemDataBytes = new byte[_size];

            Array.Copy(BitConverter.GetBytes(this.Preamble), 0, systemDataBytes, 0, sizeof(uint));
            Array.Copy(BitConverter.GetBytes(this.Length), 0, systemDataBytes, 4, sizeof(ushort));
            systemDataBytes[6] = (byte)this.ProtocolHandle;
            systemDataBytes[7] = (byte)this.PacketType;
            systemDataBytes[8] = (byte)this.OpCode;
            Array.Copy(this.Data, 0, systemDataBytes, HEADER_SIZE - sizeof(ushort), this.Data.Length);
            Array.Copy(BitConverter.GetBytes(this.CRC16), 0, systemDataBytes, systemDataBytes.Length - sizeof(ushort), sizeof(ushort));
            return systemDataBytes;
        }

        #endregion

    }
    #endregion


    #region Firmware Update Structures
    public struct FirmwareUpdateStruct
    {
        #region Public Constants

        /// <summary>
        /// Header size in bytes.
        /// </summary>
        public const ushort HEADER_SIZE = 8;

        #endregion

        #region Public Properties

        public ushort MTU;
        public byte Partition;
        public DownloadFirmwarePacketTypes BlockType;
        public ushort Length;
        public byte[] Data;
        public uint CRC;

        #endregion

        #region Public Methods
        /// <summary>
        /// Convert the <see cref="CommonProtocolStruct"/> object to byte array
        /// </summary>
        /// <returns>Result of conversion as byte array </returns>
        public byte[] ToByteArray()
        {
            var _size = this.Data.Length + HEADER_SIZE;
            if (_size > this.MTU)
                return null;

            var firmwareDataBytes = new byte[_size];

            firmwareDataBytes[0] = this.Partition;
            firmwareDataBytes[1] = (byte)this.BlockType;
            Array.Copy(BitConverter.GetBytes(this.Length), 0, firmwareDataBytes, 2, sizeof(ushort));
            Array.Copy(this.Data, 0, firmwareDataBytes, 4, this.Length);
            Array.Copy(BitConverter.GetBytes(this.CRC), 0, firmwareDataBytes, _size - 4, sizeof(uint));

            return firmwareDataBytes;
        }

        #endregion
    }

    /// <summary>
    /// Resources for firmware download thread
    /// </summary>
    public struct FirmwareUpdateResourcesStruct
    {
        /// <summary>
        /// Path to firmware binary file
        /// </summary>
        public string PathToBin;

        /// <summary>
        /// Index of flash partition to load
        /// </summary>
        public byte PartitionNumber;

        /// <summary>
        /// Maximum data count in single packet
        /// </summary>
        public ushort MTU;
    }

    #endregion

    #region EMG Protocol Structures

    /// <summary>
    /// EMG data packet structure
    /// </summary>
    public struct EmgDataPacketStruct
    {
        /// <summary>
        /// Number of data counts
        /// </summary>
        public ushort Count;

        /// <summary>
        /// Data array
        /// </summary>
        public int[] Data;

        /// <summary>
        /// Time of the first count in the current packet
        /// </summary>
        public ulong Time;

        /// <summary>
        /// Gets the <see cref="EmgDataPacketStruct"/> object from byte array
        /// </summary>
        /// <param name="data">Input raw byte array</param>
        /// <returns><see cref="EmgDataPacketStruct"/> object</returns>
        public static EmgDataPacketStruct FromByteArray(byte[] data)
        {
            if (data == null)
                throw new ArgumentException("Input data array is non-nullable value", "data");

            EmgDataPacketStruct output;

            output.Count = BitConverter.ToUInt16(data, 0);
            output.Data = new int[output.Count];
            for(int i = 0; i < output.Count; i++)
            {
                output.Data[i] = BitConverter.ToInt32(data, 2 + (4 * i));
            }
            output.Time = BitConverter.ToUInt64(data, 2 + (4 * output.Count));

            return output;
        }
    }

    #endregion

    #region Structures Helpers

    public static class StructuresHelpers
    {
        /// <summary>
        /// Get the <see cref="struct"/> object with <see cref="Marshal"/> mapping from byte array. 
        /// </summary>
        /// <param name="buffer">Input data buffer</param>
        /// <param name="objType">Output <see cref="struct"/> type</param>
        /// <returns>Deserialized <see cref="struct"/> type object</returns>
        public static object GetObjectFromBytes(byte[] buffer, Type objType)
        {
            object obj = null;
            if ((buffer != null) && (buffer.Length > 0))
            {
                IntPtr ptrObj = IntPtr.Zero;
                try
                {
                    int objSize = Marshal.SizeOf(objType);
                    if (objSize > 0)
                    {
                        if (buffer.Length < objSize)
                            throw new Exception(String.Format("Buffer smaller than needed for creation of object of type {0}", objType));

                        ptrObj = Marshal.AllocHGlobal(objSize);
                        if (ptrObj != IntPtr.Zero)
                        {
                            Marshal.Copy(buffer, 0, ptrObj, objSize);
                            obj = Marshal.PtrToStructure(ptrObj, objType);
                        }
                        else
                            throw new Exception(String.Format("Couldn't allocate memory to create object of type {0}", objType));
                    }
                }
                catch (Exception ex)
                {
                    if (Debugger.IsAttached)
                        Debugger.Break();
                }
                finally
                {
                    if (ptrObj != IntPtr.Zero)
                        Marshal.FreeHGlobal(ptrObj);
                }
            }
            return obj;
        }

        /// <summary>
        /// Get the byte array from <see cref="struct"/> object with <see cref="Marshal"/> mapping. 
        /// </summary>
        /// <param name="obj"><see cref="struct"/> object with <see cref="Marshal"/> mapping</param>
        /// <returns>Serialized <see cref="struct"/> object as byte array</returns>
        public static byte[] GetBytesFromObject(object obj)
        {
            var _size = Marshal.SizeOf(obj);
            var _bytes = new byte[_size];

            IntPtr ptr = IntPtr.Zero;
            try
            {
                ptr = Marshal.AllocHGlobal(_size);
                Marshal.StructureToPtr(obj, ptr, true);
                Marshal.Copy(ptr, _bytes, 0, _size);
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached)
                    Debugger.Break();
            }
            finally
            {
                Marshal.FreeHGlobal(ptr);
            }
            return _bytes;
        }
    }

    #endregion
}
