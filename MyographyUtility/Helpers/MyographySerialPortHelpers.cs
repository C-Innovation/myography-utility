﻿using RJCP.IO.Ports;
using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;

namespace MyographyUtility.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class MyographySerialPortHelpers
    {
        #region Dependency Properties

        #endregion

        #region Protected Members

        

        #endregion

        #region Private Members

        private SerialPortStream pMyographySerialPort;

        #endregion

        #region Public Properties

        #endregion

        #region Public Events

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MyographySerialPortHelpers(Action dataHandlerCallback)
        {
            
        }

        #endregion

        #region Public Methods

        public SerialPortStream GetMyographySerialPort()
        {
            return this.pMyographySerialPort;
        }

        /// <summary>
        /// Get available COM ports from system device explorer
        /// </summary>
        /// <returns>Available COM ports list</returns>
        public string[] GetAvailableComPorts()
        {
            return SerialPortStream.GetPortNames();
        }

        public async Task<byte[]> GetAvailableDataBytes()
        {
           
            if (this.pMyographySerialPort is null || !this.pMyographySerialPort.IsOpen)
                return null;

            //return await AsyncSerial.ReadAsync(this.pMyographySerialPort, this.pMyographySerialPort.BytesToRead);
            return await this.pMyographySerialPort.ReadAsync(this.pMyographySerialPort.BytesToRead);
        }
        public bool OpenMyographySerialPort(string comPortName )
        {
            if (this.pMyographySerialPort != null/* || this.pMyographySerialPort.IsOpen*/)
                return false;

            this.pMyographySerialPort = new SerialPortStream(comPortName);
            this.pMyographySerialPort.ReadBufferSize = 16777216; // TODO: Check, that 16 MB works well...
            this.pMyographySerialPort.OpenDirect();
            Thread.Sleep(50);
            if (!this.pMyographySerialPort.IsOpen)
                return false;

            return true;

        }

        public bool CloseMyographySerialPort()
        {
            if (this.pMyographySerialPort is null)
                return false;

            //if (!this.pMyographySerialPort.IsOpen)
            //    return false;

            this.pMyographySerialPort.Close();
            this.pMyographySerialPort.Dispose();
            this.pMyographySerialPort = null;

            return true;

        }

        public bool SendByteArray(byte[] buffer)
        {
            if (this.pMyographySerialPort is null || !this.pMyographySerialPort.IsOpen || buffer is null)
                return false;

            this.pMyographySerialPort.Write(buffer, 0, buffer.Length);

            return true;
        }

        public async Task SendByteArrayAsync(byte[] buffer)
        {
            if (this.pMyographySerialPort is null || !this.pMyographySerialPort.IsOpen || buffer is null)
                return;

            await this.pMyographySerialPort.WriteAsync(buffer, 0, buffer.Length);

            
        }

        #endregion


        #region Private Methods

        #endregion
    }
}
