﻿using System;

namespace MyographyUtility.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class SystemProtocolTestHelpers
    {
        #region Dependency Properties

        #endregion

        #region Protected Members

        #endregion

        #region Private Members

        private static byte pPingNum = 0;

        #endregion

        #region Public Properties

        #endregion

        #region Public Events

        #endregion

        #region Public Methods

        public static CommonProtocolStruct PrepareSystemProtocolData(byte opCode, byte handle, /*CommunicationProtocolTypes protocol_type,*/ byte[] data = null)
        {
            return getProtocolCommandData(handle, opCode, data);
           /* switch (command)
            {
                case SystemProtocolTypes.VERSION: return getVersionData(handle);
                case SystemProtocolTypes.INFO: return getDeviceInfoData(handle);
                case SystemProtocolTypes.STATUS: return getStatusData(handle);
                case SystemProtocolTypes.PING: return getPingData(handle);
                case SystemProtocolTypes.PROTOCOLS: return getSupportedProtocolsData(handle);
                case SystemProtocolTypes.RESET: return getResetData(handle);
                case SystemProtocolTypes.IN_BOOT: return getGoInBootData(handle);
                case SystemProtocolTypes.OPEN_PROTOCOL: return getOpenProtocolData(handle, protocol_type);
                case SystemProtocolTypes.CLOSE_PROTOCOL: return  getCloseProtocolData(handle);
                case SystemProtocolTypes.MTU: return getMtuData(handle);
                case SystemProtocolTypes.SET_TIME_CORRECTION: return getTimeCorrectionData(handle, data);
                case SystemProtocolTypes.NONE: return new CommonProtocolStruct(0);
                default: return new CommonProtocolStruct(0);
            }*/
        }

        #endregion

        #region Private Methods
/*
        private static CommonProtocolStruct getVersionData(byte protocolHandle)
        {
            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 3,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)SystemProtocolTypes.VERSION,
                Data = new byte[0]

            };
            data.CRC16 = get_crc16(data);
            return data;
        }

        private static CommonProtocolStruct getDeviceInfoData(byte protocolHandle)
        {
            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 3,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)SystemProtocolTypes.INFO,
                Data = new byte[0]

            };
            data.CRC16 = get_crc16(data);
            return data;
        }

        private static CommonProtocolStruct getStatusData(byte protocolHandle)
        {
            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 3,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)SystemProtocolTypes.STATUS,
                Data = new byte[0]

            };
            data.CRC16 = get_crc16(data);
            return data;
        }

        private static CommonProtocolStruct getPingData(byte protocolHandle)
        {
            
            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 4,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)SystemProtocolTypes.PING,
                Data = new byte[1]
            };
            data.Data[0] = pPingNum++;
            data.CRC16 = get_crc16(data);
            return data;
        }

        private static CommonProtocolStruct getSupportedProtocolsData(byte protocolHandle)
        {
            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 3,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)SystemProtocolTypes.PROTOCOLS,
                Data = new byte[0]

            };
            data.CRC16 = get_crc16(data);
            return data;
        }

        private static CommonProtocolStruct getResetData(byte protocolHandle)
        {
            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 3,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)SystemProtocolTypes.RESET,
                Data = new byte[0]

            };
            data.CRC16 = get_crc16(data);
            return data;
        }

        private static CommonProtocolStruct getGoInBootData(byte protocolHandle)
        {
            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 3,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)SystemProtocolTypes.IN_BOOT,
                Data = new byte[0]

            };
            data.CRC16 = get_crc16(data);
            return data;
        }

        private static CommonProtocolStruct getOpenProtocolData(byte protocolHandle, CommunicationProtocolTypes protocol)
        {

            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 4,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)SystemProtocolTypes.OPEN_PROTOCOL,
                Data = new byte[1]
            };
            data.Data[0] = (byte)protocol;
            data.CRC16 = get_crc16(data);
            return data;
        }

        private static CommonProtocolStruct getCloseProtocolData(byte protocolHandle)
        {

            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 4,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)SystemProtocolTypes.CLOSE_PROTOCOL,
                Data = new byte[1]
            };
            data.Data[0] = protocolHandle;
            data.CRC16 = get_crc16(data);
            return data;
        }

        private static CommonProtocolStruct getMtuData(byte protocolHandle)
        {
            CommonProtocolStruct data = new CommonProtocolStruct()
            {   
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 3,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)SystemProtocolTypes.MTU,
                Data = new byte[0]

            };
            data.CRC16 = get_crc16(data);
            return data;
        }

        private static CommonProtocolStruct getTimeCorrectionData(byte protocolHandle, byte[] timeData)
        {
            CommonProtocolStruct data = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = 7,
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = (byte)SystemProtocolTypes.SET_TIME_CORRECTION,
                Data = (timeData is null) ? new byte[0] : timeData

            };
            data.CRC16 = get_crc16(data);
            return data;
        }
*/
        private static CommonProtocolStruct getProtocolCommandData(byte protocolHandle, byte opCode, byte[] data = null)
        {
            CommonProtocolStruct commandData = new CommonProtocolStruct()
            {
                Preamble = CommonProtocolStruct.HEADER_PREAMBLE,
                Length = (ushort)((data is null) ? 3 : (data.Length + 3)),
                ProtocolHandle = protocolHandle,
                PacketType = CommunicationCommandTypes.CMD,
                OpCode = opCode,
                Data = (data is null) ? new byte[0] : data

            };
            commandData.CRC16 = get_crc16(commandData);
            return commandData;
        }


        private static ushort crc_u16(byte[] bytes)
        {
            ushort crc = 0xFFFF;
            byte i;

            for (int j = 0; j < bytes.Length; j++)
            {
                crc ^= (ushort)(bytes[j] << 8);

                for (i = 0; i < 8; i++)
                    crc = (ushort)(((crc & 0x8000) == 0x8000) ? ((crc << 1) ^ 0x1021) : (crc << 1));
            }
            return crc;



        }

        private static ushort get_crc16(CommonProtocolStruct packet)
        {
            byte[] tmp = new byte[packet.Length + 2];
            Array.Copy(packet.ToByteArray(), 4, tmp, 0, tmp.Length);
            return crc_u16(tmp);
        }


        #endregion
    }
}
