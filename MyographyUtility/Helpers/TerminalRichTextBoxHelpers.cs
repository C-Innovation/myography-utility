﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace MyographyUtility.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class TerminalRichTextBoxHelpers : RichTextBox, IDisposable
    {
        #region Dependency Properties

        public static DependencyProperty CurrentVerticalOffsetProperty =
                      DependencyProperty.Register("CurrentVerticalOffset", 
                                         typeof(double), 
                                         typeof(TerminalRichTextBoxHelpers), 
                                         new PropertyMetadata(new PropertyChangedCallback(OnVerticalChanged)));

       

        
        #endregion

        #region Protected Members

        RichTextBox mTerminalRichTextBox;

        #endregion

        #region Private Members

        #endregion

        #region Public Properties

        public double CurrentVerticalOffset
        {
            get { return (double)this.GetValue(CurrentVerticalOffsetProperty); }
            set { this.SetValue(CurrentVerticalOffsetProperty, value); }
        }

        #endregion

        #region Public Events

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public TerminalRichTextBoxHelpers()
        {

        }

        #endregion

        #region Private Callback

        private static void OnVerticalChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TerminalRichTextBoxHelpers extRtb = d as TerminalRichTextBoxHelpers;
            extRtb.ScrollToVerticalOffset((double)e.NewValue);
        }

        #endregion

        #region Command Methods

        #endregion

        #region Public Methods

        public void AppendTerminalMonitor(string text, Brush color)
        {

            TextRange rangeOfText2 = new TextRange(this.Document.ContentEnd, this.Document.ContentEnd);
            rangeOfText2.Text = text + Environment.NewLine;
            rangeOfText2.ApplyPropertyValue(TextElement.ForegroundProperty, color);// color);
            rangeOfText2.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);
            AnimateScrollRTB();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Start the scroll animation
        /// </summary>
        /// <param name="rtb"><see cref="ExtRichTextBox"/> object</param>
        private void AnimateScrollRTB()
        {
            DoubleAnimation vertAnim = new DoubleAnimation();
            vertAnim.From = this.VerticalOffset;
            vertAnim.To = this.ExtentHeight;
            vertAnim.DecelerationRatio = .2;
            vertAnim.Duration = new Duration(TimeSpan.FromMilliseconds(600));
            Storyboard sb = new Storyboard();
            sb.Children.Add(vertAnim);
            Storyboard.SetTarget(vertAnim, this);
            Storyboard.SetTargetProperty(vertAnim, new PropertyPath(TerminalRichTextBoxHelpers.CurrentVerticalOffsetProperty));
            sb.Begin();
        }

        #endregion
    }
}
