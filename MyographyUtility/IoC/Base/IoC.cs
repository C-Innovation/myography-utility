﻿using MyographyUtility.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyographyUtility
{
   
    /// <summary>
    /// 
    /// </summary>
    public static class IoC
    {
        #region Public Properties

        /// <summary>
        /// 
        /// </summary>
        public static IKernel Kernel { get; private set; } = new StandardKernel();

        /// <summary>
        /// 
        /// </summary>
        public static IUIManager UI => IoC.Get<IUIManager>();

        /// <summary>
        /// 
        /// </summary>
        public static ILogFactory Logger => IoC.Get<ILogFactory>();

        /// <summary>
        /// 
        /// </summary>
        public static IFileManager File => IoC.Get<IFileManager>();

        /// <summary>
        /// 
        /// </summary>
        public static ITaskManager Task => IoC.Get<ITaskManager>();

        /// <summary>
        /// Main <see cref="ApplicationViewModel"/> object re-translation
        /// </summary>
        public static ApplicationViewModel Application => Get<ApplicationViewModel>();



        #endregion

        #region Construction

        /// <summary>
        /// 
        /// </summary>
        public static void Setup()
        {
            //
            BindViewModels();
        }
        
        /// <summary>
        /// 
        /// </summary>
        private static void BindViewModels()
        {
            //
            Kernel.Bind<ApplicationViewModel>().ToConstant(new ApplicationViewModel());

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T Get<T>()
        {
            return Kernel.Get<T>();
        }
    }
}
