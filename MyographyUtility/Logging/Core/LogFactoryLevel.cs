﻿namespace MyographyUtility
{
    /// <summary>
    /// 
    /// </summary>
    public enum LogOutputLevel
    {
        /// <summary>
        /// 
        /// </summary>
        Debug = 1,

        /// <summary>
        /// 
        /// </summary>
        Verbose = 2,

        /// <summary>
        /// 
        /// </summary>
        Informative = 3,


        /// <summary>
        /// 
        /// </summary>
        Critical = 4,

        /// <summary>
        /// 
        /// </summary>
        Nothing = 7,
    }
}
