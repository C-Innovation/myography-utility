﻿namespace MyographyUtility
{
    /// <summary>
    /// 
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// 
        /// </summary>
        Debug = 1,

        /// <summary>
        /// 
        /// </summary>
        Verbose = 2,

        /// <summary>
        /// 
        /// </summary>
        Informative = 3,

        /// <summary>
        /// 
        /// </summary>
        Warning = 4,

        /// <summary>
        /// 
        /// </summary>
        Error = 5,

        /// <summary>
        /// 
        /// </summary>
        Success = 6,

    }
}
