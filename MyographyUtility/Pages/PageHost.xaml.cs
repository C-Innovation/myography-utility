﻿
using MyographyUtility.ValueConverters;
using MyographyUtility.ViewModels;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;


namespace MyographyUtility
{
    /// <summary>
    /// Логика взаимодействия для PageHost.xaml
    /// </summary>
    public partial class PageHost : UserControl
    {
        #region Dependency Properties

        /// <summary>
        /// 
        /// </summary>
        public ApplicationPage CurrentPage
        {
            get => (ApplicationPage)GetValue(CurrentPageProperty);
            set => SetValue(CurrentPageProperty, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty CurrentPageProperty =
            DependencyProperty.Register(nameof(CurrentPage), typeof(ApplicationPage), typeof(PageHost), new UIPropertyMetadata(default(ApplicationPage),null, CurrentPagePrpertyCanged));

        /// <summary>
        /// 
        /// </summary>
        public BaseViewModel CurrentPageViewModel
        {
            get => (BaseViewModel)GetValue(CurrentPageViewModelProperty);
            set => SetValue(CurrentPageViewModelProperty, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public static readonly DependencyProperty CurrentPageViewModelProperty =
            DependencyProperty.Register(nameof(CurrentPageViewModel), typeof(BaseViewModel), typeof(PageHost), new UIPropertyMetadata());

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public PageHost()
        {
            InitializeComponent();

            if(DesignerProperties.GetIsInDesignMode(this))
                   NewPage.Content = IoC.Application.CurrentPage.ToBasePage();

            
        }

        #endregion

        #region Property Canged Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static object CurrentPagePrpertyCanged(DependencyObject d, object value)
        {
            var currentPage = (ApplicationPage)d.GetValue(CurrentPageProperty);
            var currentPageViewModel = d.GetValue(CurrentPageViewModelProperty);

            // Get the frames
            var newPageFrame = (d as PageHost).NewPage;
            var oldPageFrame = (d as PageHost).OldPage;

            //
            //
            if (newPageFrame.Content is BasePage page &&
                page.ToApplicationPage()==currentPage)
            {
                //
                page.ViewModelObject = currentPageViewModel;

                return value;
            }


           

            //
            var oldPageContent = newPageFrame.Content;

            //
            newPageFrame.Content = null;

            //
            oldPageFrame.Content = oldPageContent;

            //
            if (oldPageContent is BasePage oldPage)
            {
                //
                oldPage.ShouldAnimateOut = true;

                //
                Task.Delay((int)(oldPage.SlideSeconds * 1000)).ContinueWith((t) =>
                {
                Application.Current.Dispatcher.Invoke(() => oldPageFrame.Content = null);
                   
                });
            }
            //
            newPageFrame.Content =  currentPage.ToBasePage(currentPageViewModel);

            return value;
        }


        #endregion
    }

}    
