﻿using MyographyUtility.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyographyUtility
{
    /// <summary>
    /// Interaction logic for ProtocolTestPage.xaml
    /// </summary>
    public partial class ProtocolTestPage : BasePage<ProtocolTestViewModel>
    {
        public ProtocolTestPage() : base()
        {
            InitializeComponent();
            this.DataContext = new ProtocolTestViewModel();
            (this.DataContext as ProtocolTestViewModel).SetParentPage(this);
        }

        public ProtocolTestPage(ProtocolTestViewModel specificProtocolTestViewModel = null) : base(specificProtocolTestViewModel)
        {
            InitializeComponent();
            (this.DataContext as ProtocolTestViewModel).SetParentPage(this);
        }
    }
}
