﻿
using MyographyUtility.ViewModels;
using System;
using System.Diagnostics;
using System.Globalization;

namespace MyographyUtility.ValueConverters
{
    /// <summary>
    /// Converts the<see cref="ApplicationPage"/> to an actual view/page
    /// </summary>
    public static class ApplicationPageHelpers
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public static BasePage ToBasePage(this ApplicationPage page, object viewModel = null)
        {
            //throw new NotImplementedException();
            switch (page)
            {
                case ApplicationPage.ProtocolTest:
                    return new ProtocolTestPage(viewModel as ProtocolTestViewModel);



                default:
                    Debugger.Break();
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static ApplicationPage ToApplicationPage(this BasePage page)
        {
            //
            if (page is ProtocolTestPage)
                return ApplicationPage.ProtocolTest;



            Debugger.Break();
            return default(ApplicationPage);
        }
    }
}
