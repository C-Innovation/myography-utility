﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MyographyUtility.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class ApplicationViewModel : BaseViewModel
    {
        /// <summary>
        /// Current Application Page
        /// </summary>
        public ApplicationPage CurrentPage { get; private set; } = ApplicationPage.ProtocolTest;

        /// <summary>
        /// The view model to use for the current page when GoToPage is called
        /// </summary>
        public BaseViewModel CurrentPageViewModel { get; set; }

        /// <summary>
        /// True if the side menu should be shown
        /// </summary>
        public bool SideMenuVisible { get; set; } = true;

        /// <summary>
        /// Ttings menu should be shown
        /// </summary>rue if the set
        public bool SettingsMenuVisible { get; set; }

        /// <summary>
        /// The name of founded serial port 
        /// </summary>
        public string MainPortName { get; set; }

        /// <summary>
        /// Scenario Folder Path
        /// </summary>
        public string ScenarioFolder { get; set; } = AppDomain.CurrentDomain.BaseDirectory + "/Scenarios";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="viewModel"></param>
        public void GoToPage(ApplicationPage page, BaseViewModel viewModel = null)
        {
            //
            SettingsMenuVisible = false;

            //
            CurrentPageViewModel = viewModel;

            

            //
            CurrentPage = page;

            //
            OnPropertyChanged(nameof(CurrentPage));

            //
            //SideMenuVisible = page == ApplicationPage.Chat;

        }
    }
}
