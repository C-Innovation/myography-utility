﻿using MyographyUtility.ApplicationExtensions;
using MyographyUtility.Helpers;
using RJCP.IO.Ports;
using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;

namespace MyographyUtility.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class MainWindowViewModel : BaseViewModel
    {
        #region Dependency Properties

        #endregion

        #region Protected Members

        #endregion

        #region Private Members

        #endregion

        #region Public Properties
             
        #endregion

        #region Public Commands

        #endregion

        #region Public Events

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public MainWindowViewModel(MainWindow mWindow)
        {
           
        }

        #endregion

        #region Private Callbacks

        #endregion

        #region Command Methods
          
        #endregion

        #region Public Methods

        #endregion

        #region Private Methods

        #endregion
    }

}
