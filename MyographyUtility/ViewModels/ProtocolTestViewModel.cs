﻿using MyographyUtility.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;



namespace MyographyUtility.ViewModels
{
    /// <summary>
    /// 
    /// </summary>
    public class ProtocolTestViewModel : BaseViewModel
    {
        private const double EMG_SAMPLE_RATE = 64000.0;
        private const double EMG_POINT_INTERVAL = 1000000.0 / EMG_SAMPLE_RATE;
        private const double EMG_POINT_LOST_COUNT = 10.0 ;

        #region Dependency Properties

        #endregion

        #region Protected Members

        protected ObservableCollection<string> mPortNamesCollection;

        protected ProtocolTestPage mMainWindow;

        #endregion

        #region Private Members

        private bool pSerialReceiveThreadActive = false;

        private bool pBitrateThreadActive = false;

        private byte pProtocolHandle = 0x00;

        private const int _pReceiveBufferSize = 16777216;

        private MyographySerialPortHelpers pMyographySerialPortHelpers;

        private InputCommandTextBoxHelpers pInputCommandTextBox;

        private Thread pSerialReceiveThread;

        private CircularBuffer<byte> pSerialReceiveBuffer;

        private Dictionary<string, double> pYGridSizes = new Dictionary<string, double>()
        {
            {"100 nV" , 100},
            {"200 nV" , 200},
            {"500 nV" , 500},
            {"1 uV" , 1000},
            {"2 uV" , 2000},
            {"5 uV" , 5000},
            {"10 uV" , 10000},
            {"20 uV" , 20000},
            {"50 uV" , 50000},
            {"100 uV" , 100000},
            {"200 uV" , 200000},
            {"500 uV" , 500000},
            {"1 mV" , 1000000},
            {"2 mV" , 2000000},
            {"5 mV" , 5000000},
            {"10 mV" , 10000000},
            {"20 mV" , 20000000},
            {"50 mV" , 50000000},
        };

        #endregion

        #region Public Properties

        public bool IsSystemProtocolViewShown { get; set; } = false;
        public bool IsBootProtocolViewShown { get; set; } = false;
        public bool IsEmgProtocolViewShown { get; set; } = false;
        public bool IsElStmProtocolViewShown { get; set; } = false;

        public int SelectedComPortIndex { get; set; } = -1;

        public ObservableCollection<string> PortNamesCollection
        {
            get => this.mPortNamesCollection;
            set
            {
                if (value is null)
                    return;

                if (this.mPortNamesCollection == value)
                    return;

                this.mPortNamesCollection = value;
            }
        }

        public Dictionary<byte, CommunicationProtocolTypes> AvailableProtocols { get; set; } = new Dictionary<byte, CommunicationProtocolTypes>();

        public PointCollection EmgPoints { get; set; }

        public LineChartPointsHelpers LineChartPointsHelpers;

        public string EmgBitrateText { get; set; }

        public string BufferLoadText { get; set; }

        public double EmgBitrate { get; set; }

        public string TimeDeltaText { get; set; }

        public double TimeDelta { get; set; }

        public double TimeOld { get; set; } = 0;
        public int MissedPacketsCount { get; set; } = 0;
        #endregion

        #region Public Commands

        public ICommand RefreshComPortListCommand { get; set; }

        public ICommand OpenComPortCommand { get; set; }

        public ICommand CloseComPortCommand { get; set; }

        public ICommand CustomCommandSendCommand { get; set; }

        public ICommand SwitchProtocolsMenuCommand { get; set; }

        #endregion

        #region Public Events

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public ProtocolTestViewModel()
        {
            this.AvailableProtocols.Add(0x01, CommunicationProtocolTypes.SYSTEM);
            
            //byte handle = findProtocolHandle(AvailableProtocols, CommunicationProtocolTypes.SYSTEM);
        }

        #endregion

        #region Private Callbacks

        private void MMainWindow_Closed(object sender, System.EventArgs e)
        {
            this.pMyographySerialPortHelpers.CloseMyographySerialPort();
            this.pSerialReceiveThreadActive = false;
            this.pBitrateThreadActive = false;
            this.pInputCommandTextBox.Dispose();
            this.mMainWindow.MainTerminal.Dispose();
        }

        private async void SystemProtocolView_OnCommandReady(object sender, SystemProtocolViewEventArgs e)
        {
            if (e.Data.PacketType == CommunicationCommandTypes.NONE ||
                (SystemProtocolTypes)e.Data.OpCode == SystemProtocolTypes.NONE)
                return;

            byte[] commandBytes = e.Data.ToByteArray();

            if (this.pMyographySerialPortHelpers.SendByteArray(commandBytes))
                printOutputBytes(commandBytes);


            if ((SystemProtocolTypes)e.Data.OpCode == SystemProtocolTypes.RESET ||
                (SystemProtocolTypes)e.Data.OpCode == SystemProtocolTypes.IN_BOOT)
            {
                string portName = this.pMyographySerialPortHelpers.GetMyographySerialPort().PortName;
                resetComPortConnection(portName);
            }

            if((SystemProtocolTypes)e.Data.OpCode == SystemProtocolTypes.OPEN_PROTOCOL)
            {
                await Task.Run(() =>
                {
                    Thread.Sleep(500);
                    if(this.pProtocolHandle > 0x00)
                    {
                        CommunicationProtocolTypes newProtocolType = (CommunicationProtocolTypes)((SystemProtocolTypes)e.Data.Data[0]);
                        if (findProtocolHandle(this.AvailableProtocols, newProtocolType) == 0)
                        {
                            this.AvailableProtocols.Add(this.pProtocolHandle, newProtocolType);
                            switch(newProtocolType)
                            {
                                case CommunicationProtocolTypes.BOOT:
                                    this.mMainWindow.BootProtocolView.ProtocolHandle = this.pProtocolHandle;
                                    break;
                                case CommunicationProtocolTypes.EMG:
                                    this.mMainWindow.EmgProtocolView.ProtocolHandle = this.pProtocolHandle;
                                    break;
                                case CommunicationProtocolTypes.EL_STM:
                                    this.mMainWindow.ElStmProtocolView.ProtocolHandle = this.pProtocolHandle;
                                    break;
                            }
                        }
                        else
                        {
                            this.mMainWindow.MainTerminal.Dispatcher.Invoke(() =>
                            {
                                printLogMessageInTerminal(string.Format("Protocol {0:S} is already opened. ", newProtocolType.ToString()), TerminalLogOutputType.WARNING);
                            });
                        }
                    }
                });
            }
        }

        private void BootProtocolView_OnCommandReady(object sender, SystemProtocolViewEventArgs e)
        {
            if (e.Data.PacketType == CommunicationCommandTypes.NONE ||
                (BootProtocolTypes)e.Data.OpCode == BootProtocolTypes.NONE)
                return;

            if((BootProtocolTypes)e.Data.ProtocolHandle == 0x00)
            {
                this.mMainWindow.MainTerminal.Dispatcher.Invoke(() =>
                {
                    printLogMessageInTerminal(string.Format("No available boot protocol opened."), TerminalLogOutputType.ERROR);
                });
                return;
            }

            byte[] commandBytes = e.Data.ToByteArray();

            if (this.pMyographySerialPortHelpers.SendByteArray(commandBytes))
                printOutputBytes(commandBytes);

            if ((BootProtocolTypes)e.Data.OpCode == BootProtocolTypes.RUN)
            {
                string portName = this.pMyographySerialPortHelpers.GetMyographySerialPort().PortName;

                resetComPortConnection(portName);
            }
        }

        private void BootProtocolView_OnDownloadFirmwareResourcesReady(object sender, FirmwareDownloadEventArgs e)
        {

            printLogMessageInTerminal($"Download Started. File: {e.FirmwareResources.PathToBin}", TerminalLogOutputType.INFO);

            new Thread(new ThreadStart(async () => await downloadFirmwareAcync(e.FirmwareResources))).Start();

        }

        private void EmgProtocolView_OnCommandReady(object sender, SystemProtocolViewEventArgs e)
        {
            if ((EmgProtocolTypes)e.Data.ProtocolHandle == 0x00)
            {
                this.mMainWindow.MainTerminal.Dispatcher.Invoke(() =>
                {
                    printLogMessageInTerminal(string.Format("No available EMG protocol opened."), TerminalLogOutputType.ERROR);
                });
                return;
            }
            byte[] commandBytes = e.Data.ToByteArray();

            if (this.pMyographySerialPortHelpers.SendByteArray(commandBytes))
                printOutputBytes(commandBytes);
        }

        private void PortsCollectoin_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (this.mPortNamesCollection.Count == 0)
            {
                this.SelectedComPortIndex = -1;
                return;
            }

            if (SelectedComPortIndex < 0)
            {
                this.SelectedComPortIndex = 0;
            }

        }

        #endregion

        #region Command Methods

        private void RefreshComPortList()
        {
            this.PortNamesCollection.Clear();
            string[] portNames = this.pMyographySerialPortHelpers.GetAvailableComPorts();
            for (int i = 0; i < portNames.Length; i++)
                this.PortNamesCollection.Add(portNames[i]);
        }

        private void OpenComPort()
        {
            if (this.mMainWindow.ComPortSelection.Items.Count == 0)
            {
                printLogMessageInTerminal("COM Port not selected!", TerminalLogOutputType.ERROR);
                return;
            }

            if (this.pMyographySerialPortHelpers.GetMyographySerialPort() != null)
            {
                printLogMessageInTerminal($"{this.pMyographySerialPortHelpers.GetMyographySerialPort().PortName} is already opened!", TerminalLogOutputType.WARNING);
                return;
            }

            if (this.pMyographySerialPortHelpers.OpenMyographySerialPort(this.mMainWindow.ComPortSelection.SelectedItem.ToString()))
            {
                this.pSerialReceiveThreadActive = true;
                this.pSerialReceiveThread = new Thread(new ThreadStart(serialReceiveThreadActionAsync));
                this.pSerialReceiveThread.Start();
                printLogMessageInTerminal($"{this.pMyographySerialPortHelpers.GetMyographySerialPort().PortName} is opened", TerminalLogOutputType.INFO);
            }
            else
                printLogMessageInTerminal($"{this.pMyographySerialPortHelpers.GetMyographySerialPort().PortName} Error!", TerminalLogOutputType.ERROR);
        }

        private void CloseComPort()
        {
            if (this.pMyographySerialPortHelpers.GetMyographySerialPort() is null)
            {
                printLogMessageInTerminal("No opened port detected!", TerminalLogOutputType.WARNING);
                return;
            }
            string _portName = this.pMyographySerialPortHelpers.GetMyographySerialPort().PortName;
            if (this.pMyographySerialPortHelpers.CloseMyographySerialPort())
            {
                this.pSerialReceiveThreadActive = false;
                printLogMessageInTerminal($"{_portName} is closed", TerminalLogOutputType.INFO);
            }
            else
                printLogMessageInTerminal($"{_portName} Error!", TerminalLogOutputType.ERROR);
        }

        private void CustomCommandSend()
        {

            byte[] commandBytes = this.pInputCommandTextBox.GetInputByteArray();

            if (this.pMyographySerialPortHelpers.SendByteArray(commandBytes))
                printOutputBytes(commandBytes);
        }

        private async Task SwitchProtocolsMenu(object parameter)
        {
            string paramStr = parameter.ToString();
            byte paramByte = 0;
            await Task.Run(() =>
            {
                paramByte = byte.Parse(paramStr);
                IsSystemProtocolViewShown = ((paramByte & 0x01) == 0x01) ? true : false;
                IsBootProtocolViewShown = ((paramByte & 0x02) == 0x02) ? true : false;
                IsEmgProtocolViewShown = ((paramByte & 0x04) == 0x04) ? true : false;
                IsElStmProtocolViewShown = ((paramByte & 0x08) == 0x08) ? true : false;
            });
        }
        #endregion

        #region Public Methods

        public void SetParentPage(ProtocolTestPage mPage)
        {
            this.mMainWindow = mPage;

            Application.Current.MainWindow.Closed += MMainWindow_Closed;

            this.pSerialReceiveBuffer = new CircularBuffer<byte>(_pReceiveBufferSize);

            this.pMyographySerialPortHelpers = new MyographySerialPortHelpers(null);

            this.PortNamesCollection = new ObservableCollection<string>();
            this.mPortNamesCollection.CollectionChanged += PortsCollectoin_CollectionChanged;

            this.RefreshComPortListCommand = new RelayCommand(RefreshComPortList);
            this.OpenComPortCommand = new RelayCommand(OpenComPort);
            this.CloseComPortCommand = new RelayCommand(CloseComPort);
            this.CustomCommandSendCommand = new RelayCommand(CustomCommandSend);

            this.SwitchProtocolsMenuCommand = new RelayParameterizedCommand(async (parameter) => await SwitchProtocolsMenu(parameter));

            //RefreshComPortList();

            this.mMainWindow.SystemProtocolView.OnCommandReady += SystemProtocolView_OnCommandReady;
            this.mMainWindow.BootProtocolView.OnCommandReady += BootProtocolView_OnCommandReady;
            this.mMainWindow.BootProtocolView.OnDownloadFirmwareResourcesReady += BootProtocolView_OnDownloadFirmwareResourcesReady;
            this.mMainWindow.EmgProtocolView.OnCommandReady += EmgProtocolView_OnCommandReady;

            this.pInputCommandTextBox = new InputCommandTextBoxHelpers(this.mMainWindow.CustomCommandTextBox);

            this.LineChartPointsHelpers = new LineChartPointsHelpers(this.mMainWindow.MainChart);
            this.mMainWindow.MainChart.SizeChanged += MainChart_SizeChanged;

            RefreshComPortList();

            this.pBitrateThreadActive = true;
            Thread bitThread = new Thread(new ThreadStart(()=>
            {
                bitrateCalc();
            }));
            bitThread.Start();

            initXGridCombobox(this.mMainWindow.YGridSizeSelection);
        }

        private void MainChart_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.EmgPoints = this.LineChartPointsHelpers.UpdatePoints();
        }



        #endregion

        #region Private Methods



        private async void serialReceiveThreadActionAsync()
        {
            while (this.pSerialReceiveThreadActive)
            {
                try
                {
                    byte[] inpitBytes = await this.pMyographySerialPortHelpers.GetAvailableDataBytes();

                    
                    if (inpitBytes != null && inpitBytes.Length > 0)
                    {
                        this.EmgBitrate += inpitBytes.Length;
                        this.pSerialReceiveBuffer.PushBackArray(inpitBytes);
                        
                       
                    }
                }
                catch (Exception ex)
                {
                    //if (Debugger.IsAttached)
                    //    Debugger.Break();
                }
                finally
                {
                    this.BufferLoadText = string.Format("Buffer Load: {0:F6} MB / 16 MB", (double)this.pSerialReceiveBuffer.Size / 1024.0 / 1024.0);
                    this.OnPropertyChanged(nameof(this.BufferLoadText));
                    while (HandleReceiveBuffer(this.pSerialReceiveBuffer)) ;
                    Thread.Sleep(10);
                }
                
            }
        }



        private async Task downloadFirmwareAcync(FirmwareUpdateResourcesStruct firmwareRes)
        {
            byte[] binBytes = File.ReadAllBytes(firmwareRes.PathToBin);
            byte handle = findProtocolHandle(AvailableProtocols, CommunicationProtocolTypes.BOOT);
            
            if (handle == 0)
                return;

            // TODO: Add dynamic MTU request
            ushort _mtu = firmwareRes.MTU;
            byte _partition = firmwareRes.PartitionNumber;
            int _sizeLeft = binBytes.Length;
            int _pointer = 0;
            FirmwareUpdateStruct firmwareData;

            this.mMainWindow.BootProtocolView.InitializeDownloadFirmwareProgress(binBytes.Length);

            //await this.pMyographySerialPortHelpers.SendByteArrayAsync(BootProtocolTestHelpers.GetClearPartitionDataBytes(_partition));
            //await Task.Delay(1000);

            firmwareData = new FirmwareUpdateStruct()
            {
                MTU = _mtu,
                Partition = _partition,
                BlockType = DownloadFirmwarePacketTypes.FIRST,
                Length = (ushort)(_sizeLeft > (_mtu - FirmwareUpdateStruct.HEADER_SIZE - 3) ? (_mtu - FirmwareUpdateStruct.HEADER_SIZE - 3) : _sizeLeft),
                Data = new byte[(_sizeLeft > (_mtu - FirmwareUpdateStruct.HEADER_SIZE - 3) ? (_mtu - FirmwareUpdateStruct.HEADER_SIZE - 3) : _sizeLeft)],
                CRC = 0
            };

            Array.Copy(binBytes, _pointer, firmwareData.Data, 0, firmwareData.Length);
            firmwareData.CRC += crc_u32(firmwareData.Data);
            await this.pMyographySerialPortHelpers.SendByteArrayAsync(BootProtocolTestHelpers.GetFirmwareUpdateDataBytes(firmwareData.ToByteArray(), handle));
            _sizeLeft -= firmwareData.Length;
            _pointer += firmwareData.Length;
            this.mMainWindow.BootProtocolView.ReportDownloadFirmwareProgress(_pointer);
            await Task.Delay(100);
            while (_sizeLeft > (_mtu - FirmwareUpdateStruct.HEADER_SIZE))
            {
                firmwareData = new FirmwareUpdateStruct()
                {
                    MTU = _mtu,
                    Partition = _partition,
                    BlockType = DownloadFirmwarePacketTypes.MIDDLE,
                    Length = (ushort)(_sizeLeft > (_mtu - FirmwareUpdateStruct.HEADER_SIZE - 3) ? (_mtu - FirmwareUpdateStruct.HEADER_SIZE - 3) : _sizeLeft),
                    Data = new byte[(_sizeLeft > (_mtu - FirmwareUpdateStruct.HEADER_SIZE - 3) ? (_mtu - FirmwareUpdateStruct.HEADER_SIZE - 3) : _sizeLeft)],
                    CRC = 0
                };

                Array.Copy(binBytes, _pointer, firmwareData.Data, 0, firmwareData.Length);
                firmwareData.CRC += crc_u32(firmwareData.Data);
                await this.pMyographySerialPortHelpers.SendByteArrayAsync(BootProtocolTestHelpers.GetFirmwareUpdateDataBytes(firmwareData.ToByteArray(), handle));
                _sizeLeft -= firmwareData.Length;
                _pointer += firmwareData.Length;
                this.mMainWindow.BootProtocolView.ReportDownloadFirmwareProgress(_pointer);
                await Task.Delay(100);
            }

            firmwareData = new FirmwareUpdateStruct()
            {
                MTU = _mtu,
                Partition = _partition,
                BlockType = DownloadFirmwarePacketTypes.LAST,
                Length = (ushort)(_sizeLeft > (_mtu - FirmwareUpdateStruct.HEADER_SIZE - 3) ? (_mtu - FirmwareUpdateStruct.HEADER_SIZE - 3) : _sizeLeft),
                Data = new byte[(_sizeLeft > (_mtu - FirmwareUpdateStruct.HEADER_SIZE - 3) ? (_mtu - FirmwareUpdateStruct.HEADER_SIZE - 3) : _sizeLeft)],
                CRC = 0
            };


            //firmwareData.CRC = crc_u32(binBytes);

            Array.Copy(binBytes, _pointer, firmwareData.Data, 0, firmwareData.Length);
            //firmwareData.CRC += crc_u32(firmwareData.Data);
            firmwareData.CRC = crc_u32(binBytes);
            await this.pMyographySerialPortHelpers.SendByteArrayAsync(BootProtocolTestHelpers.GetFirmwareUpdateDataBytes(firmwareData.ToByteArray(), handle));
            await Task.Delay(20);
            this.mMainWindow.MainTerminal.Dispatcher.Invoke(() =>
            {
                
                //resetComPortConnection(pMyographySerialPortHelpers.GetMyographySerialPort().PortName);
                CloseComPort();
                printLogMessageInTerminal(string.Format("CRC: HEX = 0x{0:X08};\t\tDEC = {0:D}", firmwareData.CRC), TerminalLogOutputType.INFO);
            });


            _sizeLeft -= firmwareData.Length;
            _pointer += firmwareData.Length;
            this.mMainWindow.BootProtocolView.ReportDownloadFirmwareProgress(_pointer);
            //TODO: Uncomment after debug
            this.mMainWindow.BootProtocolView.ReportDownloadFirmwareProgress(0);
        }

        private uint crc_u32(byte[] bytes)
        {
            uint[] crc_table = new uint[256];
            uint crc; 
            int i, j;
            for (i = 0; i < 256; i++)
            {
                crc = (uint)i;
                for (j = 0; j < 8; j++)
                    crc = ((crc & 1) == 1) ? (crc >> 1) ^ 0xEDB88320U : crc >> 1;

                crc_table[i] = crc;
            };

            crc = 0xFFFFFFFFU;
            int len = bytes.Length;
            for (int k = 0; k < bytes.Length; k++)
                crc = crc_table[(crc ^ bytes[k]) & 0xFF] ^ (crc >> 8);

            return crc ^ 0xFFFFFFFFU;

            
        }

        private ushort crc_u16(byte[] bytes)
        {
            ushort crc = 0xFFFF;
            byte i;

            for (int j = 0; j < bytes.Length; j++)
            {
                crc ^= (ushort)(bytes[j] << 8);

                for (i = 0; i < 8; i++)
                    crc = (ushort)(((crc & 0x8000) == 0x8000) ? ((crc << 1) ^ 0x1021) : (crc << 1));
            }
            return crc;



        }

        private bool HandleReceiveBuffer(CircularBuffer<byte> buffer)
        {
            if (buffer.IsEmpty || buffer.Size < 6)
                return false;

            byte[] header = buffer.FrontArray(6);

            uint preamble = (uint)(BitConverter.ToUInt32(header, 0));

            ushort size = (ushort)(BitConverter.ToUInt16(header, 4) + 8);

            if (buffer.Size < size)
                return false;

            CommonProtocolStruct receivedPacket = CommonProtocolStruct.FromByteArray(buffer.FrontArray(size));
            buffer.PopFrontArray(size);

            byte[] tmp = new byte[size - 6];
            Array.Copy(receivedPacket.ToByteArray(), 4, tmp, 0, size - 6);

            if (receivedPacket.CRC16 != crc_u16(tmp))
                return false;

            // TODO: Add association with receivedPacket.ProtocolHandle and CommunicationProtocolTypes structure
            CommunicationProtocolTypes protoocolType = getProtocolType(this.AvailableProtocols, receivedPacket.ProtocolHandle);

            if(protoocolType != CommunicationProtocolTypes.EMG)
                this.mMainWindow.MainTerminal.Dispatcher.Invoke(() =>
                {
                    printInputBytes(receivedPacket.ToByteArray());
                });

            
            switch (protoocolType)
            {
                case CommunicationProtocolTypes.RESERVED: break;
                case CommunicationProtocolTypes.SYSTEM: handleSystemProtocol(receivedPacket); break;
                case CommunicationProtocolTypes.BOOT: break;
                case CommunicationProtocolTypes.EMG: handleEmgProtocol(receivedPacket); break;
                case CommunicationProtocolTypes.EL_STM: break;
                default: break;
            }

            return true;
        }

        private async void handleSystemProtocol(CommonProtocolStruct package)
        {
            switch ((SystemProtocolTypes)package.OpCode)
            {
                case SystemProtocolTypes.MTU:
                    ushort _mtu = BitConverter.ToUInt16(package.Data, 0);
                    this.mMainWindow.BootProtocolView.SetMTU(_mtu);
                    this.mMainWindow.MainTerminal.Dispatcher.Invoke(() =>
                    {
                        printLogMessageInTerminal($"Received MTU: {_mtu}.", TerminalLogOutputType.INFO);
                    });

                    break;
                case SystemProtocolTypes.OPEN_PROTOCOL:
                    this.pProtocolHandle = package.Data[0];
                    this.mMainWindow.MainTerminal.Dispatcher.Invoke(() =>
                    {
                        printLogMessageInTerminal(string.Format("Protocol is opened. Handle: 0x{0:X02}", this.pProtocolHandle), TerminalLogOutputType.INFO);
                    });
                    
                    break;


                default: break;
            }
        }

        private async void handleEmgProtocol(CommonProtocolStruct package)
        {
            if(package.PacketType == CommunicationCommandTypes.RESP)
            {
                this.mMainWindow.MainTerminal.Dispatcher.Invoke(() =>
                {
                    printInputBytes(package.ToByteArray());
                });
            }
            else if(package.PacketType == CommunicationCommandTypes.EVENT)
            {
                switch((EmgProtocolEventTypes)package.OpCode)
                {
                    case EmgProtocolEventTypes.EMG_EVENT:
                        EmgDataPacketStruct emgData = EmgDataPacketStruct.FromByteArray(package.Data);
                        this.mMainWindow.MainChart.Dispatcher.Invoke(() =>
                        {
                            
                            //this.mMainWindow.MainChart.Series[0].Points = this.LineChartPointsHelpers.CalculatePoints(emgData.Data);
                            this.EmgPoints = this.LineChartPointsHelpers.CalculatePoints(emgData.Data);
                            //this.EmgBitrateText = $"T: {emgData.Time} us";
                            this.OnPropertyChanged(nameof(this.EmgPoints));

                            if(this.TimeOld == 0)
                            {
                                this.TimeOld = (double)emgData.Time + ((double)emgData.Count * EMG_POINT_INTERVAL);
                            }
                            else
                            {
                                this.TimeDelta = emgData.Time - this.TimeOld;
                                this.TimeOld = (double)emgData.Time + ((double)emgData.Count * EMG_POINT_INTERVAL);
                                if (Math.Abs(this.TimeDelta / EMG_POINT_INTERVAL) >= EMG_POINT_LOST_COUNT)
                                    this.MissedPacketsCount++;
                                this.TimeDeltaText = string.Format("Delta Time: {0:F3} us; Missed packets: {1:D}.", this.TimeDelta, this.MissedPacketsCount);
                                this.OnPropertyChanged(nameof(this.TimeDeltaText));
                            }
                            //this.OnPropertyChanged(nameof(this.EmgBitrateText));
                        });
                        
                        break;

                    case EmgProtocolEventTypes.RX_EVENT:
                        this.mMainWindow.MainTerminal.Dispatcher.Invoke(() =>
                        {
                            printInputBytes(package.ToByteArray());
                        });
                        break;

                    default : break;
                }
            }
        }

        private CommunicationProtocolTypes getProtocolType(Dictionary<byte, CommunicationProtocolTypes> availableProtocols, byte handle)
        {
            if (availableProtocols.ContainsKey(handle))
                return availableProtocols[handle];
            else
                return CommunicationProtocolTypes.NONE;
        }


        private byte findProtocolHandle(Dictionary<byte, CommunicationProtocolTypes> availableProtocols, CommunicationProtocolTypes type)
        {
            return availableProtocols.FirstOrDefault(x => x.Value == type).Key;
        }


        private void resetComPortConnection(string portName)
        {
            CloseComPort();
            //while(this.pMyographySerialPortHelpers.GetMyographySerialPort() != null)
            Thread.Sleep(2000);
            while (!new Collection<string>(this.pMyographySerialPortHelpers.GetAvailableComPorts()).Contains(portName))
            {
                Thread.Sleep(100);
            }
            this.AvailableProtocols.Clear();
            this.AvailableProtocols.Add(0x01, CommunicationProtocolTypes.SYSTEM);
            this.mMainWindow.BootProtocolView.ProtocolHandle = 0x00;
            this.mMainWindow.EmgProtocolView.ProtocolHandle = 0x00;
            OpenComPort();
        }
        private void printOutputBytes(byte[] buffer)
        {
            string outputStr = "<SENT>\t\t";
            for (int bufIndex = 0; bufIndex < buffer.Length; bufIndex++)
                outputStr += string.Format("{0:X02} ", buffer[bufIndex]);

            this.mMainWindow.MainTerminal.AppendTerminalMonitor(outputStr, Brushes.Green);
        }

        private void printInputBytes(byte[] buffer)
        {
            string outputStr = "<RECV>\t\t";
            for (int bufIndex = 0; bufIndex < buffer.Length; bufIndex++)
                outputStr += string.Format("{0:X02} ", buffer[bufIndex]);

            this.mMainWindow.MainTerminal.AppendTerminalMonitor(outputStr, Brushes.Purple);
        }

        private void printLogMessageInTerminal(string message, TerminalLogOutputType type)
        {
            switch (type)
            {
                case TerminalLogOutputType.ERROR:
                    this.mMainWindow.MainTerminal.AppendTerminalMonitor($"<FAIL>\t\t{message}", Brushes.Red);
                    break;
                case TerminalLogOutputType.WARNING:
                    this.mMainWindow.MainTerminal.AppendTerminalMonitor($"<WARN>\t{message}", Brushes.Orange);
                    break;
                case TerminalLogOutputType.INFO:
                    this.mMainWindow.MainTerminal.AppendTerminalMonitor($"<INFO>\t\t{message}", Brushes.Blue);
                    break;
            }
        }

        private void bitrateCalc()
        {
            while (this.pBitrateThreadActive)
            {
                Thread.Sleep(1000);
                //this.mMainWindow.MainChart.Dispatcher.Invoke(() =>
                //{
                this.EmgBitrateText = string.Format("Data rate: {0:F3} kBps", this.EmgBitrate / 1024.0);
                
                this.EmgBitrate = 0;
                //this.EmgBitrateText = $"T: {emgData.Time} us";
                this.OnPropertyChanged(nameof(this.EmgBitrateText));
                
                //});
            }

        }

        private void initXGridCombobox(ComboBox yGridSelection)
        {
            foreach (KeyValuePair<string, double> kvp in this.pYGridSizes)
            {
                ComboBoxItem comboBoxItem = new ComboBoxItem();

                comboBoxItem.Content = kvp.Key;
                yGridSelection.Items.Add(comboBoxItem);
            }

            
            

            yGridSelection.SelectionChanged += XGridSelection_SelectionChanged;
            yGridSelection.SelectedIndex = 6;
        }

        private void XGridSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //KeyValuePair<string, double> kvp = this.pXGridSizes[e.Source];
            this.LineChartPointsHelpers.SetYGridSize(this.pYGridSizes[((ComboBoxItem)e.AddedItems[0]).Content.ToString()]);
            this.EmgPoints = this.LineChartPointsHelpers.UpdatePoints();
            this.OnPropertyChanged(nameof(this.EmgPoints));
        }

        #endregion
    }



    public enum TerminalLogOutputType
    {
        ERROR,
        WARNING,
        INFO,
    }
}
