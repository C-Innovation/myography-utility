﻿
using MyographyUtility.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyographyUtility
{
    /// <summary>
    /// 
    /// </summary>
    public class ViewModelLocator
    {
        #region Public Properties

        /// <summary>
        /// 
        /// </summary>
        public static ViewModelLocator Instance { get; private set; } = new ViewModelLocator();

        /// <summary>
        /// 
        /// </summary>
        public static ApplicationViewModel ApplicationViewModel => IoC.Application;

        
        #endregion
    }
}
